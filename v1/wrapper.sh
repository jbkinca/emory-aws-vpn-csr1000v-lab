#
#        Name: wrapper.sh
# Description: bash wrapper script for provisioning multiple AWS IPSEC VPNs in the virtual lab environment.
#              Calls easvpnctl.py, but values to arguments are only valid in the lab.
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#

#
# $1 = Action to perform - <add | delete | status>
#
# $2 = First VPC in the loop - (integer >= 1)
#
# $3 = Last VPC in the loop - (integer <= 200, can be same as $2 if only touching a single VPC)
#

#
# edit these to match your setup
#
Host1="r1"
Host2="r2"
user="admin"
password="cisco"

#
# Don't edit below this line
#
for i in `seq $2 $3`; do
  PresharedKey="test`printf "%03d\n" ${i}`"
  VpcId="vpc-`printf "%03d\n" ${i}`"
  echo "Provisioning VPC $i Tunnel 1 - $1"
  python easvpnctl.py -a ${Host1} -u ${user} -p ${password} --RemoteVpnIpAddress 10.21.209.225 --PresharedKey ${PresharedKey} --VpcId ${VpcId} --VpcNum ${i} --action ${1}

  echo "Provisioning VPC $i Tunnel 2 - $1"
  python easvpnctl.py -a ${Host2} -u ${user} -p ${password} --RemoteVpnIpAddress 10.21.209.225 --PresharedKey ${PresharedKey} --VpcId ${VpcId} --VpcNum ${i} --action ${1}
done

