#
#        Name: easvpnctl.py
# Description: Emory AWS Service VPN Control python script used for turning vpn's up/down via NETCONF/YANG.
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#
import sys
import lxml.etree as ET
import time
from argparse import ArgumentParser
from ncclient import manager
from ncclient.operations import RPCError

if __name__ == '__main__':

    # Number of configuration retry attempts
    ConfigRetries = 50
    # Number of configuration lock retry attempts
    LockRetries = 50
    # Number of seconds to sleep between configuration attempts
    RetryCooldown = 10

    parser = ArgumentParser(description='Usage:')

    # script arguments
    parser.add_argument('-a', '--host', type=str, required=True,
                        help="Device IP address or Hostname.  Allowed values are: <r1 | r2")
    parser.add_argument('-u', '--username', type=str, required=True,
                        help="Device Username (netconf agent username)")
    parser.add_argument('-p', '--password', type=str, required=True,
                        help="Device Password (netconf agent password)")
    parser.add_argument('--port', type=int, default=830,
                        help="Netconf agent port")
    parser.add_argument('--RemoteVpnIpAddress', type=str, required=True,
                        help="AWS VPC VPN endpoint IP address")
    parser.add_argument('--PresharedKey', type=str, required=True,
                        help="AWS VPC VPN pre-shared key")
    parser.add_argument('--VpcId', type=str, required=True,
                        help="AWS VPC identification string i.e. vpc-xxxxxxx")
    parser.add_argument('--VpcNum', type=int, required=True,
                        help="CIDR Service VPC Number <1-200>")
    parser.add_argument('--action', type=str, required=True,
                        help="Action to take <add | delete | status>")
    parser.add_argument('--verbose', type=str, required=False,
                        help="Set to 1 for more detailed output")
    args = parser.parse_args()

    #
    # RPC to determine Cisco-IOS-XE-tunnel schema version 
    #
    RPC_Tunnel_Schema = """
    <filter>
      <netconf-state xmlns="urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring">
        <schemas>
          <schema>
            <identifier>Cisco-IOS-XE-tunnel</identifier>
          </schema>
        </schemas>
      </netconf-state>
    </filter>
    """

    # tunnel destination YANG model changed
    # older model
    Tunnel_Destination_YANG_1_add = """<ios-tun:destination>FIXME_RemoteVpnIpAddress</ios-tun:destination>"""
    Tunnel_Destination_YANG_1_delete = """<ios-tun:destination xc:operation="remove"></ios-tun:destination>"""

    # newer model
    Tunnel_Destination_YANG_2_add = """<ios-tun:destination>
                  <ios-tun:ipaddress-or-host>FIXME_RemoteVpnIpAddress</ios-tun:ipaddress-or-host>
                </ios-tun:destination>"""
    Tunnel_Destination_YANG_2_delete = """<ios-tun:destination>
                  <ios-tun:ipaddress-or-host xc:operation="remove"></ios-tun:ipaddress-or-host>
                </ios-tun:destination>"""

    if args.action == "add":
      #
      # Generic RPC template for "--action add"
      #
      RPC_Add = """
      <config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
        <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp">
          <crypto>
            <ios-crypto:keyring>
              <ios-crypto:name>keyring-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
              <ios-crypto:vrf>AWS</ios-crypto:vrf>
              <ios-crypto:description>FIXME_VpcId</ios-crypto:description>
              <ios-crypto:pre-shared-key>
                <ios-crypto:address>
                  <ios-crypto:ipv4>
                    <ios-crypto:ipv4-addr>FIXME_RemoteVpnIpAddress</ios-crypto:ipv4-addr>
                    <ios-crypto:key xc:operation="create"/>
                    <ios-crypto:unencryt-key>FIXME_PresharedKey</ios-crypto:unencryt-key>
                  </ios-crypto:ipv4>
                </ios-crypto:address>
              </ios-crypto:pre-shared-key>
            </ios-crypto:keyring>
            <ios-crypto:isakmp>
              <ios-crypto:profile>
                <ios-crypto:name>isakmp-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
                <ios-crypto:description>FIXME_VpcId</ios-crypto:description>
                <ios-crypto:match>
                  <ios-crypto:identity>
                    <ios-crypto:ipv4-address>
                      <ios-crypto:address>FIXME_RemoteVpnIpAddress</ios-crypto:address>
                      <ios-crypto:mask>255.255.255.255</ios-crypto:mask>
                      <ios-crypto:vrf>AWS</ios-crypto:vrf>
                    </ios-crypto:ipv4-address>
                  </ios-crypto:identity>
                </ios-crypto:match>
              </ios-crypto:profile>
            </ios-crypto:isakmp>
            <ios-crypto:ipsec>
              <ios-crypto:transform-set xc:operation="create">
                <ios-crypto:tag>ipsec-prop-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:tag>
                <ios-crypto:esp>esp-aes</ios-crypto:esp>
                <ios-crypto:key-bit>256</ios-crypto:key-bit>
                <ios-crypto:esp-hmac>esp-sha256-hmac</ios-crypto:esp-hmac>
                <ios-crypto:mode>
                  <ios-crypto:tunnel xc:operation="create"/>
                </ios-crypto:mode>
              </ios-crypto:transform-set>
              <ios-crypto:profile xc:operation="create">
                <ios-crypto:name>ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
                <ios-crypto:description>FIXME_VpcId</ios-crypto:description>
                <ios-crypto:set>
                  <ios-crypto:pfs>
                    <ios-crypto:group>group2</ios-crypto:group>
                  </ios-crypto:pfs>
                  <ios-crypto:transform-set>ipsec-prop-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:transform-set>
                  <ios-crypto:security-association>
                    <ios-crypto:lifetime>
                      <ios-crypto:seconds>3600</ios-crypto:seconds>
                    </ios-crypto:lifetime>
                  </ios-crypto:security-association>
                </ios-crypto:set>
              </ios-crypto:profile>
            </ios-crypto:ipsec>
          </crypto>
          <interface>
            <Tunnel>
              <name>FIXME_TunnelNum0FIXME_VpcNum</name>
              <description>AWS Research VPCFIXME_VpcNum TunnelFIXME_TunnelNum (FIXME_VpcId)</description>
              <ios-tun:tunnel>
                FIXME_TUNNEL_DESTINATION_YANG
                <ios-tun:protection>
                  <ios-crypto:ipsec>
                    <ios-crypto:profile>ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:profile>
                  </ios-crypto:ipsec>
                </ios-tun:protection>
              </ios-tun:tunnel>
              <shutdown xc:operation="remove"/>
            </Tunnel>
          </interface>
          <router>
            <ios-bgp:bgp>
              <ios-bgp:id>65000</ios-bgp:id>
              <ios-bgp:address-family>
                <ios-bgp:with-vrf>
                  <ios-bgp:ipv4>
                    <ios-bgp:af-name>unicast</ios-bgp:af-name>
                    <ios-bgp:vrf>
                      <ios-bgp:name>AWS</ios-bgp:name>
                      <ios-bgp:neighbor>
                        <ios-bgp:id>FIXME_BgpNeighborId</ios-bgp:id>
                        <ios-bgp:description>FIXME_VpcId</ios-bgp:description>
                      </ios-bgp:neighbor>
                    </ios-bgp:vrf>
                  </ios-bgp:ipv4>
                </ios-bgp:with-vrf>
              </ios-bgp:address-family>
            </ios-bgp:bgp>
          </router>
        </native>
      </config>
      """
    elif args.action == "delete":
      #
      # Generic RPC templates for "--action delete"
      #
      RPC_Tunnel_Shutdown = """
      <config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
        <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel">
          <interface>
            <Tunnel>
              <name>FIXME_TunnelNum0FIXME_VpcNum</name>
              <shutdown xc:operation="create"/>
            </Tunnel>
          </interface>
        </native>
      </config>
      """

      RPC_Delete = """
      <config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
        <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp">
          <router>
            <ios-bgp:bgp>
              <ios-bgp:id>65000</ios-bgp:id>
              <ios-bgp:address-family>
                <ios-bgp:with-vrf>
                  <ios-bgp:ipv4>
                    <ios-bgp:af-name>unicast</ios-bgp:af-name>
                    <ios-bgp:vrf>
                      <ios-bgp:name>AWS</ios-bgp:name>
                      <ios-bgp:neighbor>
                        <ios-bgp:id>FIXME_BgpNeighborId</ios-bgp:id>
                        <ios-bgp:description>AVAILABLE</ios-bgp:description>
                      </ios-bgp:neighbor>
                    </ios-bgp:vrf>
                  </ios-bgp:ipv4>
                </ios-bgp:with-vrf>
              </ios-bgp:address-family>
            </ios-bgp:bgp>
          </router>
          <interface>
            <Tunnel>
              <name>FIXME_TunnelNum0FIXME_VpcNum</name>
              <description>AWS Research VPCFIXME_VpcNum TunnelFIXME_TunnelNum (AVAILABLE)</description>
              <ios-tun:tunnel>
                FIXME_TUNNEL_DESTINATION_YANG
                <ios-tun:protection>
                  <ios-crypto:ipsec>
                    <ios-crypto:profile xc:operation="remove">ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:profile>
                  </ios-crypto:ipsec>
                </ios-tun:protection>
              </ios-tun:tunnel>
            </Tunnel>
          </interface>
          <crypto>
            <ios-crypto:keyring>
              <ios-crypto:name>keyring-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
              <ios-crypto:vrf>AWS</ios-crypto:vrf>
              <ios-crypto:description xc:operation="remove"></ios-crypto:description>
              <ios-crypto:pre-shared-key>
                <ios-crypto:address>
                  <ios-crypto:ipv4 xc:operation="remove">
                    <ios-crypto:ipv4-addr>FIXME_RemoteVpnIpAddress</ios-crypto:ipv4-addr>
                    <ios-crypto:key/>
                    <ios-crypto:unencryt-key>FIXME_PresharedKey</ios-crypto:unencryt-key>
                  </ios-crypto:ipv4>
                </ios-crypto:address>
              </ios-crypto:pre-shared-key>
            </ios-crypto:keyring>
            <ios-crypto:isakmp>
              <ios-crypto:profile>
                <ios-crypto:name>isakmp-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
                <ios-crypto:description xc:operation="remove"></ios-crypto:description>
                <ios-crypto:match>
                  <ios-crypto:identity>
                    <ios-crypto:ipv4-address xc:operation="remove">
                      <ios-crypto:address>FIXME_RemoteVpnIpAddress</ios-crypto:address>
                      <ios-crypto:mask>255.255.255.255</ios-crypto:mask>
                      <ios-crypto:vrf>AWS</ios-crypto:vrf>
                    </ios-crypto:ipv4-address>
                  </ios-crypto:identity>
                </ios-crypto:match>
              </ios-crypto:profile>
            </ios-crypto:isakmp>
            <ios-crypto:ipsec>
              <ios-crypto:transform-set xc:operation="remove">
                <ios-crypto:tag>ipsec-prop-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:tag>
              </ios-crypto:transform-set>
              <ios-crypto:profile xc:operation="remove">
                <ios-crypto:name>ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
              </ios-crypto:profile>
            </ios-crypto:ipsec>
          </crypto>
        </native>
      </config>
      """

    elif args.action == "status":
      #
      # Generic RPC template for "--action status"
      #
      RPC_Status = """
      <filter>
        <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp">
          <crypto>
            <ios-crypto:ipsec>
              <ios-crypto:profile>
                <ios-crypto:name>ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
              </ios-crypto:profile>
              <ios-crypto:transform-set>
                <ios-crypto:tag>ipsec-prop-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:tag>
              </ios-crypto:transform-set>
            </ios-crypto:ipsec>
            <ios-crypto:isakmp>
              <ios-crypto:profile>
                <ios-crypto:name>isakmp-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
              </ios-crypto:profile>
            </ios-crypto:isakmp>
            <ios-crypto:keyring>
              <ios-crypto:name>keyring-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>
            </ios-crypto:keyring>
          </crypto>
          <interface>
            <Tunnel>
              <name>FIXME_TunnelNum0FIXME_VpcNum</name>
            </Tunnel>
          </interface>
          <router>
            <ios-bgp:bgp>
              <ios-bgp:id>65000</ios-bgp:id>
              <ios-bgp:address-family>
                <ios-bgp:with-vrf>
                  <ios-bgp:ipv4>
                    <ios-bgp:af-name>unicast</ios-bgp:af-name>
                    <ios-bgp:vrf>
                      <ios-bgp:name>AWS</ios-bgp:name>
                      <ios-bgp:neighbor>
                        <ios-bgp:id>FIXME_BgpNeighborId</ios-bgp:id>
                      </ios-bgp:neighbor>
                    </ios-bgp:vrf>
                  </ios-bgp:ipv4>
                </ios-bgp:with-vrf>
              </ios-bgp:address-family>
            </ios-bgp:bgp>
          </router>
        </native>
        <bgp-state-data xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp-oper">
          <neighbors>
            <neighbor>
              <afi-safi>vpnv4-unicast</afi-safi>
              <vrf-name>AWS</vrf-name>
              <neighbor-id>FIXME_BgpNeighborId</neighbor-id>
              <description/>
              <up-time/>
              <installed-prefixes/>
              <session-state/>
              <prefix-activity>
                <sent>
                  <current-prefixes/>
                </sent>
                <received>
                  <current-prefixes/>
                </received>
              </prefix-activity>
            </neighbor>
          </neighbors>
        </bgp-state-data>
        <interfaces-state xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
            <interface>
              <name>TunnelFIXME_TunnelNum0FIXME_VpcNum</name>
              <admin-status/>
              <oper-status/>
            </interface>
          </interfaces-state>
      </filter>
      """

    #
    # Determine Tunnel Number based on host
    #
    TunnelNum = "0"
    if args.host.lower().startswith("r1"):
      TunnelNum = "1"
    if args.host.lower().startswith("r2"):
      TunnelNum = "2"
    if TunnelNum == "0":
      sys.exit("Invalid HOST argument provided.  Run with -h for valid options.") 

    # create padded VpcNum str i.e. 001, 002, etc.
    VpcNum=str(args.VpcNum).zfill(3)

    #
    # Calculate BgpNeighborId (VpnInsideCidr + 1)
    #
    VpnInsideCidr_Oct4 = 0
    if TunnelNum == "1":
      # For Tunnel 1, VpnInsideCidr's start at 169.254.248.0
      VpnInsideCidr_Oct3 = 248
    elif TunnelNum == "2":
      # For Tunnel 2, VpnInsideCidr's start at 169.254.252.0
      VpnInsideCidr_Oct3 = 252

    for x in range(1, args.VpcNum):
       VpnInsideCidr_Oct4 = VpnInsideCidr_Oct4 + 4
       if VpnInsideCidr_Oct4 == 256:
         VpnInsideCidr_Oct4 = 0
         VpnInsideCidr_Oct3 = VpnInsideCidr_Oct3 + 1

    BgpNeighborId_Oct3 = VpnInsideCidr_Oct3
    BgpNeighborId_Oct4 = VpnInsideCidr_Oct4 + 1
    BgpNeighborId = "169.254." + str(BgpNeighborId_Oct3) + "." + str(BgpNeighborId_Oct4)

    # print verbose info
    if args.verbose > 0:
      print(args.host)
      print(VpcNum)
      print(TunnelNum)
      print(args.RemoteVpnIpAddress)
      print(args.PresharedKey)
      print(BgpNeighborId)

    # connect to netconf agent
    with manager.connect(host=args.host,
                         port=args.port,
                         username=args.username,
                         password=args.password,
                         timeout=90,
                         hostkey_verify=False,
                         device_params={'name': 'csr'}) as m:

        #
        # Determine Cisco-IOS-XE-tunnel schema version
        #
        try:
            if args.verbose > 0:
              print(RPC_Tunnel_Schema)
            # Get - can retrieve config data as well as device state data
            response = m.get(filter=RPC_Tunnel_Schema).xml
            data = ET.fromstring(response)
            if args.verbose > 0:
                print(ET.tostring(data, pretty_print=True))
        except RPCError as e:
            data = e._raw
            print(ET.tostring(data, pretty_print=True))
            print e.message
            sys.exit("ERROR 4 - Unable to determine Cisco-IOS-XE-tunnel schema version.") 

        TunnelSchemaVer = data[0][0][0][0][1].text
        if args.verbose > 0:
            print TunnelSchemaVer

        #
        # select correct RPC structure based on schema version
        #
        if TunnelSchemaVer == "2017-08-28":
            Tunnel_Destination_YANG_add = Tunnel_Destination_YANG_2_add
            Tunnel_Destination_YANG_delete = Tunnel_Destination_YANG_2_delete
        elif TunnelSchemaVer == "2017-07-11":
            Tunnel_Destination_YANG_add = Tunnel_Destination_YANG_1_add
            Tunnel_Destination_YANG_delete = Tunnel_Destination_YANG_1_delete
        else:
            # default to the newest schema
            Tunnel_Destination_YANG_add = Tunnel_Destination_YANG_2_add
            Tunnel_Destination_YANG_delete = Tunnel_Destination_YANG_2_delete

        #
        # perform string replacements in RPC templates to make them specific to a VPN
        #
        if args.action == "add":
          RPC_Add=RPC_Add.replace("FIXME_TUNNEL_DESTINATION_YANG", Tunnel_Destination_YANG_add)
          RPC_Add=RPC_Add.replace("FIXME_RemoteVpnIpAddress", args.RemoteVpnIpAddress)
          RPC_Add=RPC_Add.replace("FIXME_PresharedKey", args.PresharedKey)
          RPC_Add=RPC_Add.replace("FIXME_VpcId", args.VpcId)
          RPC_Add=RPC_Add.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Add=RPC_Add.replace("FIXME_VpcNum", VpcNum)
          RPC_Add=RPC_Add.replace("FIXME_BgpNeighborId", BgpNeighborId)

        if args.action == "delete":
          RPC_Tunnel_Shutdown=RPC_Tunnel_Shutdown.replace("FIXME_RemoteVpnIpAddress", args.RemoteVpnIpAddress)
          RPC_Tunnel_Shutdown=RPC_Tunnel_Shutdown.replace("FIXME_PresharedKey", args.PresharedKey)
          RPC_Tunnel_Shutdown=RPC_Tunnel_Shutdown.replace("FIXME_VpcId", args.VpcId)
          RPC_Tunnel_Shutdown=RPC_Tunnel_Shutdown.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Tunnel_Shutdown=RPC_Tunnel_Shutdown.replace("FIXME_VpcNum", VpcNum)
          RPC_Tunnel_Shutdown=RPC_Tunnel_Shutdown.replace("FIXME_BgpNeighborId", BgpNeighborId)

          RPC_Delete=RPC_Delete.replace("FIXME_TUNNEL_DESTINATION_YANG", Tunnel_Destination_YANG_delete)
          RPC_Delete=RPC_Delete.replace("FIXME_RemoteVpnIpAddress", args.RemoteVpnIpAddress)
          RPC_Delete=RPC_Delete.replace("FIXME_PresharedKey", args.PresharedKey)
          RPC_Delete=RPC_Delete.replace("FIXME_VpcId", args.VpcId)
          RPC_Delete=RPC_Delete.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Delete=RPC_Delete.replace("FIXME_VpcNum", VpcNum)
          RPC_Delete=RPC_Delete.replace("FIXME_BgpNeighborId", BgpNeighborId)

        if args.action == "status":
          RPC_Status=RPC_Status.replace("FIXME_RemoteVpnIpAddress", args.RemoteVpnIpAddress)
          RPC_Status=RPC_Status.replace("FIXME_PresharedKey", args.PresharedKey)
          RPC_Status=RPC_Status.replace("FIXME_VpcId", args.VpcId)
          RPC_Status=RPC_Status.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Status=RPC_Status.replace("FIXME_VpcNum", VpcNum)
          RPC_Status=RPC_Status.replace("FIXME_BgpNeighborId", BgpNeighborId)

        # execute netconf operation
        if args.action == "add":

            # Edit Config - lock and edit running config with rollback-on-error
            lt = 1
            RC1 = 0
            while lt <= LockRetries:
                if RC1 > 0:
                    print "Operation failed. Sleeping for",RC1,"seconds until next retry."
                time.sleep(RC1)
                try:
                    RC2 = 0
                    print "Attempting to lock running configuration - retry",lt,"of",LockRetries,""
                    with m.locked('running'):
                        print "Running configuration lock obtained."
                        rt = 1
                        while rt <= ConfigRetries:
                            if RC2 > 0:
                                print "Operation failed. Sleeping for",RC2,"seconds until next retry."
                            time.sleep(RC2)
                            print "Configuring VPN - retry",rt,"of",ConfigRetries,""
                            try:
                                # Configure the VPN
                                if args.verbose > 0:
                                    print(RPC_Add)
                                response = m.edit_config(target='running', error_option='rollback-on-error', config=RPC_Add).xml
                                data = ET.fromstring(response)
                                rt = ConfigRetries + 1
                                RC2 = 0
                                print "[SUCCESS]"
                                print(ET.tostring(data, pretty_print=True))
                            except RPCError as e:
                                data = e._raw
                                rt=rt+1
                                RC2 = RetryCooldown
                                print "[ERROR 2]"
                                print(ET.tostring(data, pretty_print=True))
                                print e.message
                                if e.tag == "data-exists":
                                  break
                            except:
                                print "[Unexpected ERROR]"
                                print(ET.tostring(data, pretty_print=True))
                                print e.message
                                break

                    lt = LockRetries + 1
    
                except RPCError as e:
                    data = e._raw
                    lt=lt+1
                    RC1 = RetryCooldown
                    print "[ERROR 1 - Unable to obtain running configuration lock]"
                    print(ET.tostring(data, pretty_print=True))
                    print e.message

        elif args.action == "delete":

            # Edit Config - lock and edit running config with rollback-on-error
            lt = 1
            RC1 = 0
            while lt <= LockRetries:
                if RC1 > 0:
                    print "Operation failed. Sleeping for",RC1,"seconds until next retry."
                time.sleep(RC1)
                try:
                    RC2 = 0
                    print "Attempting to lock running configuration - retry",lt,"of",LockRetries,""
                    with m.locked('running'):
                        print "Running configuration lock obtained."
                        rt = 1
                        while rt <= ConfigRetries:
                            if RC2 > 0:
                                print "Operation failed. Sleeping for",RC2,"seconds until next retry."
                            time.sleep(RC2)
                            print "Shutting down tunnel interface - retry",rt,"of",ConfigRetries,""
                            try:
                                # shutdown the tunnel interface
                                if args.verbose > 0:
                                    print(RPC_Tunnel_Shutdown)
                                response = m.edit_config(target='running', error_option='rollback-on-error', config=RPC_Tunnel_Shutdown).xml
                                data = ET.fromstring(response)
                                rt = ConfigRetries + 1
                                RC2 = 0
                                print "[SUCCESS]"
                                print(ET.tostring(data, pretty_print=True))
                            except RPCError as e:
                                data = e._raw
                                rt=rt+1
                                RC2 = RetryCooldown
                                print "[ERROR 2]"
                                print(ET.tostring(data, pretty_print=True))
                                print e.message
                                if e.tag == "data-exists":
                                  break

                            except:
                                print "[Unexpected ERROR]"
                                print(ET.tostring(data, pretty_print=True))
                                print e.message
                                break

                        if RC2 == 0:
                            # only execute this section if tunnel interface shutdown was successful, otherwise this section will fail
                            rt = 1
                            while rt <= ConfigRetries:
                                if RC2 > 0:
                                    print "Operation failed. Sleeping for",RC2,"seconds until next retry."
                                time.sleep(RC2)
                                print "Resetting VPN configuration to defaults - retry ",rt,"of",ConfigRetries,""
                                try:
                                    # reset the vpn configuration back to defaults
                                    if args.verbose > 0:
                                        print(RPC_Delete)
                                    response = m.edit_config(target='running', error_option='rollback-on-error', config=RPC_Delete).xml
                                    data = ET.fromstring(response)
                                    rt = ConfigRetries + 1
                                    RC2 = 0
                                    print "[SUCCESS]"
                                    print(ET.tostring(data, pretty_print=True))
                                except RPCError as e:
                                    data = e._raw
                                    rt=rt+1
                                    RC2 = RetryCooldown
                                    print "[ERROR 3]"
                                    print(ET.tostring(data, pretty_print=True))
                                    print e.message
                                    if e.tag == "data-exists":
                                      break
                                except:
                                    print "[Unexpected ERROR]"
                                    print(ET.tostring(data, pretty_print=True))
                                    break

                    lt = LockRetries + 1

                except RPCError as e:
                    data = e._raw
                    lt=lt+1
                    RC1 = RetryCooldown
                    print "[ERROR 1 - Unable to obtain running configuration lock]"
                    print(ET.tostring(data, pretty_print=True))
                    print e.message

        if args.action == "status":
          try:
              if args.verbose > 0:
                print(RPC_Status)
              # Get - can retrieve config data as well as device state data
              response = m.get(filter=RPC_Status).xml
              data = ET.fromstring(response)
              print(ET.tostring(data, pretty_print=True))
          except RPCError as e:
              data = e._raw
              print(ET.tostring(data, pretty_print=True))
              print e.message


