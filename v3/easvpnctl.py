#
#        Name: easvpnctl.py
# Description: Emory AWS Service VPN Control python script used for turning vpn's up/down via NETCONF/YANG.
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#
import sys
import lxml.etree as ET
import time
from argparse import ArgumentParser
from ncclient import manager
from ncclient.operations import RPCError

if __name__ == '__main__':

    # Number of configuration retry attempts
    ConfigRetries = 50
    # Number of configuration lock retry attempts
    LockRetries = 50
    # Number of seconds to sleep between configuration attempts
    RetryCooldown = 10

    parser = ArgumentParser(description='Usage:')

    # script arguments
    parser.add_argument('-a', '--host', type=str, required=True,
                        help="Device IP address or Hostname.  Allowed values are: <r1 | r2")
    parser.add_argument('-u', '--username', type=str, required=True,
                        help="Device Username (netconf agent username)")
    parser.add_argument('-p', '--password', type=str, required=True,
                        help="Device Password (netconf agent password)")
    parser.add_argument('--port', type=int, default=830,
                        help="Netconf agent port")
    parser.add_argument('--Tunnels', type=str, required=False, default="AB",
                        help="Tunnels to act upon <A | B | AB> (defaults to AB)")
    parser.add_argument('--LocalVpnIpAddressA', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel A")
    parser.add_argument('--LocalVpnIpAddressB', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel B")
    parser.add_argument('--RemoteVpnIpAddressA', type=str, required=False,
                        help="AWS VPC VPN endpoint IP address Tunnel A")
    parser.add_argument('--RemoteVpnIpAddressB', type=str, required=False,
                        help="AWS VPC VPN endpoint IP address Tunnel B")
    parser.add_argument('--PresharedKeyA', type=str, required=False,
                        help="AWS VPC VPN pre-shared key Tunnel A")
    parser.add_argument('--PresharedKeyB', type=str, required=False,
                        help="AWS VPC VPN pre-shared key Tunnel B")
    parser.add_argument('--VpcId', type=str, required=True,
                        help="AWS VPC identification string i.e. vpc-xxxxxxx")
    parser.add_argument('--VpcNum', type=int, required=True,
                        help="CIDR Service VPC Number <1-200>")
    parser.add_argument('--MyBgpAsn', type=str, required=False, default="65000",
                        help="BGP Autonomous System Number for R1 and R2 (defaults to 65000)")
    parser.add_argument('--action', type=str, required=True,
                        help="Action to take <add | delete | status>")
    parser.add_argument('--verbose', type=str, required=False,
                        help="Set to 1 for more detailed output")
    args = parser.parse_args()

    def RPC_Template(action, tunnels = "AB"):
        #
        # Construct correct RPC structure from template based on schema version
        #
        CONFIG_HEADER = """<config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">"""
        CONFIG_FOOTER = """</config>"""
        GET_HEADER = """<filter>"""
        GET_FOOTER = """</filter>"""

        # default to latest supported version
        # current is a symbolic link
        IOS_XE_VERSION = "current"

        #YANG model schema versions (IOS-XE 16.6.2):
        #Cisco-IOS-XE-tunnel: 2017-07-11
        #Cisco-IOS-XE-crypto: 2017-05-10
        #Cisco-IOS-XE-native: 2017-08-30
        #Cisco-IOS-XE-bgp: 2017-04-28

        if TunnelSchemaVer == "2017-07-11" and CryptoSchemaVer == "2017-05-10" and NativeSchemaVer == "2017-08-30" and BgpSchemaVer == "2017-04-28":
            IOS_XE_VERSION = "1662"

        #YANG model schema versions (IOS-XE 16.8.2):
        #Cisco-IOS-XE-tunnel: 2017-08-28
        #Cisco-IOS-XE-crypto: 2018-07-10
        #Cisco-IOS-XE-native: 2018-05-22
        #Cisco-IOS-XE-bgp: 2018-01-10

        if TunnelSchemaVer == "2017-08-28" and CryptoSchemaVer == "2018-07-10" and NativeSchemaVer == "2018-05-22" and BgpSchemaVer == "2018-01-10":
            IOS_XE_VERSION = "1682"

        #YANG model schema versions (IOS-XE 16.9.4):
        #Cisco-IOS-XE-tunnel: 2017-08-28
        #Cisco-IOS-XE-crypto: 2019-04-25
        #Cisco-IOS-XE-native: 2018-07-27
        #Cisco-IOS-XE-bgp: 2019-01-09

        if TunnelSchemaVer == "2017-08-28" and CryptoSchemaVer == "2019-04-25" and NativeSchemaVer == "2018-07-27" and BgpSchemaVer == "2019-01-09":
            IOS_XE_VERSION = "1694"

        # print verbose info
        if args.verbose > 0:
            print "IOS-XE Version: " + IOS_XE_VERSION

        if action == "add":
            RPC_HEADER = CONFIG_HEADER
            RPC_FOOTER = CONFIG_FOOTER

        elif action == "shutdown":
            RPC_HEADER = CONFIG_HEADER
            RPC_FOOTER = CONFIG_FOOTER

        elif action == "delete":
            RPC_HEADER = CONFIG_HEADER
            RPC_FOOTER = CONFIG_FOOTER

        elif action == "status":
            RPC_HEADER = GET_HEADER
            RPC_FOOTER = GET_FOOTER

        #
        # Build RPC
        #
        RPC = RPC_HEADER

        if tunnels == "A" or tunnels == "AB":
            f = open("RPC-Templates/" + IOS_XE_VERSION + "/" + action + "/TunnelA","r")

            if f.mode == 'r':
                RPC = RPC + f.read()
                f.close()
            else:
                sys.exit("Unable to open RPC template file")

        if tunnels == "B" or tunnels == "AB":
            f = open("RPC-Templates/" + IOS_XE_VERSION + "/" + action + "/TunnelB","r")

            if f.mode == 'r':
                RPC = RPC + f.read()
                f.close()

            else:
                sys.exit("Unable to open RPC template file")

        RPC = RPC + RPC_FOOTER

        return RPC

    #
    # Determine Tunnel Number based on host
    #
    TunnelNum = "0"
    if args.host.lower().startswith("r1"):
      TunnelNum = "1"
    if args.host.lower().startswith("r2"):
      TunnelNum = "2"
    if TunnelNum == "0":
      sys.exit("Invalid HOST argument provided.  Run with -h for valid options.") 

    # create padded VpcNum str i.e. 001, 002, etc.
    VpcNum=str(args.VpcNum).zfill(3)

    #
    # Calculate BgpNeighborId (VpnInsideCidr + 1)
    #
    VpnInsideCidr_Oct4 = 0
    if TunnelNum == "1":
      # For Tunnel 1A, VpnInsideCidr's start at 169.254.248.0
      VpnInsideCidrA_Oct3 = 248
      # For Tunnel 1B, VpnInsideCidr's start at 169.254.248.0
      VpnInsideCidrB_Oct3 = 240
    elif TunnelNum == "2":
      # For Tunnel 2A, VpnInsideCidr's start at 169.254.252.0
      VpnInsideCidrA_Oct3 = 252
      # For Tunnel 2B, VpnInsideCidr's start at 169.254.252.0
      VpnInsideCidrB_Oct3 = 244

    for x in range(1, args.VpcNum):
       VpnInsideCidr_Oct4 = VpnInsideCidr_Oct4 + 4
       if VpnInsideCidr_Oct4 == 256:
         VpnInsideCidr_Oct4 = 0
         VpnInsideCidrA_Oct3 = VpnInsideCidrA_Oct3 + 1
         VpnInsideCidrB_Oct3 = VpnInsideCidrB_Oct3 + 1

    BgpNeighborIdA_Oct3 = VpnInsideCidrA_Oct3
    BgpNeighborIdB_Oct3 = VpnInsideCidrB_Oct3
    BgpNeighborId_Oct4 = VpnInsideCidr_Oct4 + 1
    BgpNeighborIdA = "169.254." + str(BgpNeighborIdA_Oct3) + "." + str(BgpNeighborId_Oct4)
    BgpNeighborIdB = "169.254." + str(BgpNeighborIdB_Oct3) + "." + str(BgpNeighborId_Oct4)

    # print verbose info
    if args.verbose > 0:
      print(args.host)
      print(VpcNum)
      print(TunnelNum)
      print(args.LocalVpnIpAddressA)
      print(args.LocalVpnIpAddressB)
      print(args.RemoteVpnIpAddressA)
      print(args.RemoteVpnIpAddressB)
      print(args.PresharedKeyA)
      print(args.PresharedKeyB)
      print(BgpNeighborIdA)
      print(BgpNeighborIdB)

    # connect to netconf agent
    with manager.connect(host=args.host,
                         port=args.port,
                         username=args.username,
                         password=args.password,
                         timeout=90,
                         hostkey_verify=False,
                         device_params={'name': 'csr'}) as m:

        #
        # Determine Cisco-IOS-XE-* schema versions
        #
        try:

            #
            # RPC to determine relevant Cisco-IOS-XE-* schema versions
            #
            f = open("RPC-Templates/Schemas","r")
            if f.mode == 'r':
                RPC_Schemas = "<filter>" + f.read() + "</filter>"
                f.close()
            else:
                sys.exit("Unable to open RPC template file")

            if args.verbose > 0:
              print(RPC_Schemas)

            # Get - can retrieve config data as well as device state data
            response = m.get(filter=RPC_Schemas).xml
            data = ET.fromstring(response)
            if args.verbose > 0:
                print(ET.tostring(data, pretty_print=True))

        except RPCError as e:
            data = e._raw
            #print(ET.tostring(data, pretty_print=True))
            print e.message
            sys.exit("ERROR 4 - Unable to obtain Cisco-IOS-XE-* schema versions.") 

        for schema in data[0][0][0]:
            SchemaIdentifier=schema[0].text
            SchemaVersion=schema[1].text
            SchemaFormat=schema[2].text
            SchemaNamespace=schema[3].text

            if SchemaIdentifier == "Cisco-IOS-XE-tunnel":
                TunnelSchemaVer = SchemaVersion
            elif SchemaIdentifier == "Cisco-IOS-XE-crypto":
                CryptoSchemaVer = SchemaVersion
            elif SchemaIdentifier == "Cisco-IOS-XE-native":
                NativeSchemaVer = SchemaVersion
            elif SchemaIdentifier == "Cisco-IOS-XE-bgp":
                BgpSchemaVer = SchemaVersion

        if args.verbose > 0:
            print "YANG model schema versions:"
            print "Cisco-IOS-XE-tunnel: " + TunnelSchemaVer
            print "Cisco-IOS-XE-crypto: " + CryptoSchemaVer
            print "Cisco-IOS-XE-native: " + NativeSchemaVer
            print "Cisco-IOS-XE-bgp: " + BgpSchemaVer

        #
        # perform string replacements in RPC templates to make them specific to a VPN
        #
        if args.action == "add":
          RPC_Add = RPC_Template("add", args.Tunnels)
          RPC_Add = RPC_Add.replace("FIXME_LocalVpnIpAddressA", args.LocalVpnIpAddressA)
          RPC_Add = RPC_Add.replace("FIXME_LocalVpnIpAddressB", args.LocalVpnIpAddressB)
          RPC_Add = RPC_Add.replace("FIXME_RemoteVpnIpAddressA", args.RemoteVpnIpAddressA)
          RPC_Add = RPC_Add.replace("FIXME_RemoteVpnIpAddressB", args.RemoteVpnIpAddressB)
          RPC_Add = RPC_Add.replace("FIXME_PresharedKeyA", args.PresharedKeyA)
          RPC_Add = RPC_Add.replace("FIXME_PresharedKeyB", args.PresharedKeyB)
          RPC_Add = RPC_Add.replace("FIXME_VpcId", args.VpcId)
          RPC_Add = RPC_Add.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Add = RPC_Add.replace("FIXME_VpcNum", VpcNum)
          RPC_Add = RPC_Add.replace("FIXME_BgpNeighborIdA", BgpNeighborIdA)
          RPC_Add = RPC_Add.replace("FIXME_BgpNeighborIdB", BgpNeighborIdB)
          RPC_Add = RPC_Add.replace("FIXME_MyBgpAsn", args.MyBgpAsn)
          #print RPC_Add
          #sys.exit("ADD")

        if args.action == "delete":
          RPC_Tunnel_Shutdown = RPC_Template("shutdown", args.Tunnels)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_LocalVpnIpAddressA", args.LocalVpnIpAddressA)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_LocalVpnIpAddressB", args.LocalVpnIpAddressB)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_RemoteVpnIpAddressA", args.RemoteVpnIpAddressA)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_RemoteVpnIpAddressB", args.RemoteVpnIpAddressB)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_PresharedKeyA", args.PresharedKeyA)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_PresharedKeyB", args.PresharedKeyB)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_VpcId", args.VpcId)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_VpcNum", VpcNum)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_BgpNeighborIdA", BgpNeighborIdA)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_BgpNeighborIdB", BgpNeighborIdB)
          RPC_Tunnel_Shutdown = RPC_Tunnel_Shutdown.replace("FIXME_MyBgpAsn", args.MyBgpAsn)
          #print RPC_Tunnel_Shutdown
   
          RPC_Delete = RPC_Template("delete", args.Tunnels)
          RPC_Delete = RPC_Delete.replace("FIXME_LocalVpnIpAddressA", args.LocalVpnIpAddressA)
          RPC_Delete = RPC_Delete.replace("FIXME_LocalVpnIpAddressB", args.LocalVpnIpAddressB)
          RPC_Delete = RPC_Delete.replace("FIXME_RemoteVpnIpAddressA", args.RemoteVpnIpAddressA)
          RPC_Delete = RPC_Delete.replace("FIXME_RemoteVpnIpAddressB", args.RemoteVpnIpAddressB)
          RPC_Delete = RPC_Delete.replace("FIXME_PresharedKeyA", args.PresharedKeyA)
          RPC_Delete = RPC_Delete.replace("FIXME_PresharedKeyB", args.PresharedKeyB)
          RPC_Delete = RPC_Delete.replace("FIXME_VpcId", args.VpcId)
          RPC_Delete = RPC_Delete.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Delete = RPC_Delete.replace("FIXME_VpcNum", VpcNum)
          RPC_Delete = RPC_Delete.replace("FIXME_BgpNeighborIdA", BgpNeighborIdA)
          RPC_Delete = RPC_Delete.replace("FIXME_BgpNeighborIdB", BgpNeighborIdB)
          RPC_Delete = RPC_Delete.replace("FIXME_MyBgpAsn", args.MyBgpAsn)
          #print RPC_Delete
          #sys.exit("Delete")

        if args.action == "status":
          RPC_Status = RPC_Template("status", args.Tunnels)
          RPC_Status=RPC_Status.replace("FIXME_LocalVpnIpAddressA", args.LocalVpnIpAddressA)
          RPC_Status=RPC_Status.replace("FIXME_LocalVpnIpAddressB", args.LocalVpnIpAddressB)
          RPC_Status=RPC_Status.replace("FIXME_RemoteVpnIpAddressA", args.RemoteVpnIpAddressA)
          RPC_Status=RPC_Status.replace("FIXME_RemoteVpnIpAddressB", args.RemoteVpnIpAddressB)
          RPC_Status=RPC_Status.replace("FIXME_PresharedKeyA", args.PresharedKeyA)
          RPC_Status=RPC_Status.replace("FIXME_PresharedKeyB", args.PresharedKeyB)
          RPC_Status=RPC_Status.replace("FIXME_VpcId", args.VpcId)
          RPC_Status=RPC_Status.replace("FIXME_TunnelNum", TunnelNum)
          RPC_Status=RPC_Status.replace("FIXME_VpcNum", VpcNum)
          RPC_Status=RPC_Status.replace("FIXME_BgpNeighborIdA", BgpNeighborIdA)
          RPC_Status=RPC_Status.replace("FIXME_BgpNeighborIdB", BgpNeighborIdB)
          RPC_Status=RPC_Status.replace("FIXME_MyBgpAsn", args.MyBgpAsn)
          #print RPC_Status
          #sys.exit("STATUS")

        # execute netconf operation
        if args.action == "add":

            # Edit Config - lock and edit running config with rollback-on-error
            lt = 1
            RC1 = 0
            while lt <= LockRetries:
                if RC1 > 0:
                    print "Operation failed. Sleeping for",RC1,"seconds until next retry."
                time.sleep(RC1)
                try:
                    RC2 = 0
                    print "Attempting to lock running configuration - retry",lt,"of",LockRetries,""
                    with m.locked('running'):
                        print "Running configuration lock obtained."
                        rt = 1
                        while rt <= ConfigRetries:
                            if RC2 > 0:
                                print "Operation failed. Sleeping for",RC2,"seconds until next retry."
                            time.sleep(RC2)
                            print "Configuring VPN - retry",rt,"of",ConfigRetries,""
                            try:
                                # Configure the VPN
                                if args.verbose > 0:
                                    print(RPC_Add)
                                response = m.edit_config(target='running', error_option='rollback-on-error', config=RPC_Add).xml
                                data = ET.fromstring(response)
                                rt = ConfigRetries + 1
                                RC2 = 0
                                print "[SUCCESS]"
                                #print(ET.tostring(data, pretty_print=True))
                            except RPCError as e:
                                data = e._raw
                                rt=rt+1
                                RC2 = RetryCooldown
                                print "[ERROR 2]"
                                print(ET.tostring(data, pretty_print=True))
                                print e.message
                                if e.tag == "data-exists":
                                  break
                            except:
                                print "[Unexpected ERROR]"
                                #print(ET.tostring(data, pretty_print=True))
                                print e.message
                                break

                    lt = LockRetries + 1
    
                except RPCError as e:
                    data = e._raw
                    lt=lt+1
                    RC1 = RetryCooldown
                    print "[ERROR 1 - Unable to obtain running configuration lock]"
                    #print(ET.tostring(data, pretty_print=True))
                    print e.message

        elif args.action == "delete":

            # Edit Config - lock and edit running config with rollback-on-error
            lt = 1
            RC1 = 0
            while lt <= LockRetries:
                if RC1 > 0:
                    print "Operation failed. Sleeping for",RC1,"seconds until next retry."
                time.sleep(RC1)
                try:
                    RC2 = 0
                    print "Attempting to lock running configuration - retry",lt,"of",LockRetries,""
                    with m.locked('running'):
                        print "Running configuration lock obtained."
                        rt = 1
                        while rt <= ConfigRetries:
                            if RC2 > 0:
                                print "Operation failed. Sleeping for",RC2,"seconds until next retry."
                            time.sleep(RC2)
                            print "Shutting down tunnel interface - retry",rt,"of",ConfigRetries,""
                            try:
                                # shutdown the tunnel interface
                                if args.verbose > 0:
                                    print(RPC_Tunnel_Shutdown)
                                response = m.edit_config(target='running', error_option='rollback-on-error', config=RPC_Tunnel_Shutdown).xml
                                data = ET.fromstring(response)
                                rt = ConfigRetries + 1
                                RC2 = 0
                                print "[SUCCESS]"
                                #print(ET.tostring(data, pretty_print=True))
                            except RPCError as e:
                                data = e._raw
                                rt=rt+1
                                RC2 = RetryCooldown
                                print "[ERROR 2]"
                                print(ET.tostring(data, pretty_print=True))
                                print e.message
                                if e.tag == "data-exists":
                                  break

                            except:
                                print "[Unexpected ERROR]"
                                #print(ET.tostring(data, pretty_print=True))
                                print e.message
                                break

                        if RC2 == 0:
                            # only execute this section if tunnel interface shutdown was successful, otherwise this section will fail
                            rt = 1
                            while rt <= ConfigRetries:
                                if RC2 > 0:
                                    print "Operation failed. Sleeping for",RC2,"seconds until next retry."
                                time.sleep(RC2)
                                print "Resetting VPN configuration to defaults - retry ",rt,"of",ConfigRetries,""
                                try:
                                    # reset the vpn configuration back to defaults
                                    if args.verbose > 0:
                                        print(RPC_Delete)
                                    response = m.edit_config(target='running', error_option='rollback-on-error', config=RPC_Delete).xml
                                    data = ET.fromstring(response)
                                    rt = ConfigRetries + 1
                                    RC2 = 0
                                    print "[SUCCESS]"
                                    #print(ET.tostring(data, pretty_print=True))
                                except RPCError as e:
                                    data = e._raw
                                    rt=rt+1
                                    RC2 = RetryCooldown
                                    print "[ERROR 3]"
                                    #print(ET.tostring(data, pretty_print=True))
                                    print e.message
                                    if e.tag == "data-exists":
                                      break
                                except:
                                    print "[Unexpected ERROR]"
                                    print(ET.tostring(data, pretty_print=True))
                                    break

                    lt = LockRetries + 1

                except RPCError as e:
                    data = e._raw
                    lt=lt+1
                    RC1 = RetryCooldown
                    print "[ERROR 1 - Unable to obtain running configuration lock]"
                    #print(ET.tostring(data, pretty_print=True))
                    print e.message

        if args.action == "status":
          try:
              if args.verbose > 0:
                print(RPC_Status)
              # Get - can retrieve config data as well as device state data
              response = m.get(filter=RPC_Status).xml
              data = ET.fromstring(response)
              print(ET.tostring(data, pretty_print=True))
          except RPCError as e:
              data = e._raw
              #print(ET.tostring(data, pretty_print=True))
              print e.message



