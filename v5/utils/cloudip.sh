#!/bin/bash

if [ "${1}" == "CSV" ]; then
  OUTPUT_FORMAT="CSV"
elif [ "${1}" == "SQL" ]; then
  OUTPUT_FORMAT="SQL"
else
  echo "Please specifiy the output format <SQL | CSV>."
  exit
fi

let x=1

for i in `seq 65 66`; do
  for j in `seq 0 2 254`; do
    VPC_NET[$x]="10.${i}.${j}.0/23"
    x=$((x+1))
  done
done

let x=1
let oct3A=248
let oct3B=240
let oct4=0
while [ $x -le 200 ]; do

  VPN_CIDR1A[$x]="169.254.${oct3A}.${oct4}/30"
  VPN_CIDR1B[$x]="169.254.${oct3B}.${oct4}/30"

  x=$((x+1))
  oct4=$((oct4+4))

  if [ $oct4 -gt 252 ]; then
    oct3A=$((oct3A+1))
    oct3B=$((oct3B+1))
    oct4=0
  fi
done

let x=1
let oct3A=252
let oct3B=244
let oct4=0
while [ $x -le 200 ]; do

  VPN_CIDR2A[$x]="169.254.${oct3A}.${oct4}/30"
  VPN_CIDR2B[$x]="169.254.${oct3B}.${oct4}/30"

  x=$((x+1))
  oct4=$((oct4+4))

  if [ $oct4 -gt 252 ]; then
    oct3A=$((oct3A+1))
    oct3B=$((oct3B+1))
    oct4=0
  fi
done

#let x=1
#let y=0

#for i in `seq 65 66`; do
  #for j in `seq 0 8 248`; do
    #NATL[$x]="10.${i}.${j}.0/21"
    #NATL[$((x+1))]="10.${i}.${j}.0/21"
    #NATL[$((x+2))]="10.${i}.${j}.0/21"
    #NATL[$((x+3))]="10.${i}.${j}.0/21"
#
    #NATG[$x]="170.140.143.${y}"
    #NATG[$((x+1))]="170.140.143.${y}"
    #NATG[$((x+2))]="170.140.143.${y}"
    #NATG[$((x+3))]="170.140.143.${y}"
    #x=$((x+4))
    #y=$((y+1))
  #done
#done

let j=1
E="DEV"
if [ ${OUTPUT_FORMAT} == "CSV" ]; then
  echo VpnConnectionProfileId,VpcCidr,CustomerGatewayIpAddress_1,VpnInsideIpCidr_1A,VpnInsideIpCidr_1B,TUNNEL_DESCRIPTION_1,CRYPTO_KEYRING_1,ISAKMP_PROFILE_1,IPSEC_TRANSFORM_SET_1,IPSEC_PROFILE_1,CustomerGatewayIpAddress_2,VpnInsideIpCidr_2A,VpnInsideIpCidr_2B,TUNNEL_DESCRIPTION_2,CRYPTO_KEYRING_2,ISAKMP_PROFILE_2,IPSEC_TRANSFORM_SET_2,IPSEC_PROFILE_2
fi
while [ $j -le 200 ]; do
  VPC_NUM="`seq -w $j 200 200`"
  CGW1_IP[$j]="170.140.76.${j}"
  CGW2_IP[$j]="170.140.77.${j}"
  TUN_INT_1[$j]="10${VPC_NUM}"
  TUN_INT_2[$j]="20${VPC_NUM}"
  TUNNEL_DESCRIPTION_1="AWS Research VPC${VPC_NUM} Tunnel1"
  CRYPTO_KEYRING_1="keyring-vpn-research-vpc${VPC_NUM}-tun1"
  ISAKMP_PROFILE_1="isakmp-vpn-research-vpc${VPC_NUM}-tun1"
  IPSEC_TRANSFORM_SET_1="ipsec-prop-vpn-research-vpc${VPC_NUM}-tun1"
  IPSEC_PROFILE_1="ipsec-vpn-research-vpc${VPC_NUM}-tun1"

  TUNNEL_DESCRIPTION_2="AWS Research VPC${VPC_NUM} Tunnel2"
  CRYPTO_KEYRING_2="keyring-vpn-research-vpc${VPC_NUM}-tun2"
  ISAKMP_PROFILE_2="isakmp-vpn-research-vpc${VPC_NUM}-tun2"
  IPSEC_TRANSFORM_SET_2="ipsec-prop-vpn-research-vpc${VPC_NUM}-tun2"
  IPSEC_PROFILE_2="ipsec-vpn-research-vpc${VPC_NUM}-tun2"

  #if [ $j -eq 21 ]; then
    #E="QA"
  #fi
  #if [ $j -eq 61 ]; then
    #E="PROD"
  #fi
  if [ ${OUTPUT_FORMAT} == "CSV" ]; then
    echo ${j},${VPC_NET[$j]},${CGW1_IP[$j]},${VPN_CIDR1A[$j]},${VPN_CIDR1B[$j]},${TUNNEL_DESCRIPTION_1},${CRYPTO_KEYRING_1},${ISAKMP_PROFILE_1},${IPSEC_TRANSFORM_SET_1},${IPSEC_PROFILE_1},${CGW2_IP[$j]},${VPN_CIDR2A[$j]},${VPN_CIDR2B[$j]},${TUNNEL_DESCRIPTION_2},${CRYPTO_KEYRING_2},${ISAKMP_PROFILE_2},${IPSEC_TRANSFORM_SET_2},${IPSEC_PROFILE_2}
  elif [ ${OUTPUT_FORMAT} == "SQL" ]; then
    #
    # Example SQL
    #
    #INSERT INTO VPN_CONNECTION_PROFILE (VPN_CONNECTION_PROFILE_ID, VPC_NETWORK) VALUES ('1','172.18.0.0/23');
    #INSERT INTO VPN_CNNCTN_PRFL_TUNNEL_PROFILE  (VPN_CONNECTION_PROFILE_ID, TUNNEL_ID, CRYPTO_KEYRING_NAME, ISAKAMP_PROFILE_NAME, IPSEC_TRANSFORM_SET_NAME, IPSEC_PROFILE_NAME, TUNNEL_DESCRIPTION, CUSTOMER_GATEWAY_IP, VPN_INSIDE_IP_CIDR_1, VPN_INSIDE_IP_CIDR_2) VALUES ('1','10001','keyring-vpn-research-vpc1-tun1','isakmp-vpn-research-vpc1-tun1','ipsec-prop-vpn-research-vpc1-tun1','ipsec-vpn-research-vpc1-tun1','AWS Research VPC1 Tunnel1','168.6.0.1','169.254.248.0/30');
    #INSERT INTO VPN_CNNCTN_PRFL_TUNNEL_PROFILE  (VPN_CONNECTION_PROFILE_ID, TUNNEL_ID, CRYPTO_KEYRING_NAME, ISAKAMP_PROFILE_NAME, IPSEC_TRANSFORM_SET_NAME, IPSEC_PROFILE_NAME, TUNNEL_DESCRIPTION, CUSTOMER_GATEWAY_IP, VPN_INSIDE_IP_CIDR_1, VPN_INSIDE_IP_CIDR_2) VALUES ('2','10002','keyring-vpn-research-vpc2-tun1','isakmp-vpn-research-vpc2-tun1','ipsec-prop-vpn-research-vpc2-tun1','ipsec-vpn-research-vpc2-tun1','AWS Research VPC2 Tunnel1','168.6.0.2','169.254.248.4/30');
    echo "/*******************/"
    echo "/* VPN PROFILE ${VPC_NUM} */"
    echo "INSERT INTO VPN_CONNECTION_PROFILE (VPN_CONNECTION_PROFILE_ID, VPC_NETWORK) VALUES ('${j}','${VPC_NET[${j}]}');"
    echo "INSERT INTO VPN_CNNCTN_PRFL_TUNNEL_PROFILE  (VPN_CONNECTION_PROFILE_ID, TUNNEL_ID, CRYPTO_KEYRING_NAME, ISAKAMP_PROFILE_NAME, IPSEC_TRANSFORM_SET_NAME, IPSEC_PROFILE_NAME, TUNNEL_DESCRIPTION, CUSTOMER_GATEWAY_IP, VPN_INSIDE_IP_CIDR_1, VPN_INSIDE_IP_CIDR_2) VALUES ('${j}','${TUN_INT_1[${j}]}','${CRYPTO_KEYRING_1}','${ISAKMP_PROFILE_1}','${IPSEC_TRANSFORM_SET_1}','${IPSEC_PROFILE_1}','${TUNNEL_DESCRIPTION_1}','${CGW1_IP[$j]}','${VPN_CIDR1A[$j]}','${VPN_CIDR1B[$j]}');"
    echo "INSERT INTO VPN_CNNCTN_PRFL_TUNNEL_PROFILE  (VPN_CONNECTION_PROFILE_ID, TUNNEL_ID, CRYPTO_KEYRING_NAME, ISAKAMP_PROFILE_NAME, IPSEC_TRANSFORM_SET_NAME, IPSEC_PROFILE_NAME, TUNNEL_DESCRIPTION, CUSTOMER_GATEWAY_IP, VPN_INSIDE_IP_CIDR_1, VPN_INSIDE_IP_CIDR_2) VALUES ('${j}','${TUN_INT_2[${j}]}','${CRYPTO_KEYRING_2}','${ISAKMP_PROFILE_2}','${IPSEC_TRANSFORM_SET_2}','${IPSEC_PROFILE_2}','${TUNNEL_DESCRIPTION_2}','${CGW2_IP[$j]}','${VPN_CIDR2A[$j]}','${VPN_CIDR2B[$j]}');"
  fi
  j=$((j+1))
done
