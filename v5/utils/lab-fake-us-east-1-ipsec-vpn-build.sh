#!/bin/bash

CSP_AWS=50
CSP_GCP=49
CSP_MS=48

CSP_VPN_ENDPOINT_AWS1A="10.1${CSP_AWS}.0."
CSP_VPN_ENDPOINT_AWS2A="10.2${CSP_AWS}.0."
CSP_VPN_ENDPOINT_AWS1B="10.1${CSP_AWS}.1."
CSP_VPN_ENDPOINT_AWS2B="10.2${CSP_AWS}.1."

CSP_VPN_ENDPOINT_GCP1A="10.1${CSP_GCP}.0."
CSP_VPN_ENDPOINT_GCP2A="10.2${CSP_GCP}.0."
CSP_VPN_ENDPOINT_GCP1B="10.1${CSP_GCP}.1."
CSP_VPN_ENDPOINT_GCP2B="10.2${CSP_GCP}.1."

CSP_VPN_ENDPOINT_MS1A="10.1${CSP_MS}.0."
CSP_VPN_ENDPOINT_MS2A="10.2${CSP_MS}.0."
CSP_VPN_ENDPOINT_MS1B="10.1${CSP_MS}.1."
CSP_VPN_ENDPOINT_MS2B="10.2${CSP_MS}.1."

let TUN1A_IP_OCT3=248
let TUN1B_IP_OCT3=240
let TUN2A_IP_OCT3=252
let TUN2B_IP_OCT3=244
let TUN_IP_OCT4=1

let VPC_NET_OCT2=65
let VPC_NET_OCT3=0

let EMORY_VPN_IP_OCT4=1

for VPC_NUM in `seq -w 1 200`; do
  if [ ${TUN_IP_OCT4} -gt 254 ]; then
    TUN1A_IP_OCT3=$((${TUN1A_IP_OCT3}+1))
    TUN2A_IP_OCT3=$((${TUN2A_IP_OCT3}+1))
    TUN1B_IP_OCT3=$((${TUN1B_IP_OCT3}+1))
    TUN2B_IP_OCT3=$((${TUN2B_IP_OCT3}+1))
    TUN_IP_OCT4=1
  fi
  TUN1A_IP="169.254.${TUN1A_IP_OCT3}.${TUN_IP_OCT4}"
  TUN2A_IP="169.254.${TUN2A_IP_OCT3}.${TUN_IP_OCT4}"
  TUN1B_IP="169.254.${TUN1B_IP_OCT3}.${TUN_IP_OCT4}"
  TUN2B_IP="169.254.${TUN2B_IP_OCT3}.${TUN_IP_OCT4}"
  E_TUN1A_IP="169.254.${TUN1A_IP_OCT3}.$((${TUN_IP_OCT4}+1))"
  E_TUN2A_IP="169.254.${TUN2A_IP_OCT3}.$((${TUN_IP_OCT4}+1))"
  E_TUN1B_IP="169.254.${TUN1B_IP_OCT3}.$((${TUN_IP_OCT4}+1))"
  E_TUN2B_IP="169.254.${TUN2B_IP_OCT3}.$((${TUN_IP_OCT4}+1))"

  if [ ${VPC_NET_OCT3} -gt 254 ]; then
    VPC_NET_OCT2=$((${VPC_NET_OCT2}+1))
    VPC_NET_OCT3=0
  fi
  VPC_NET="10.${VPC_NET_OCT2}.${VPC_NET_OCT3}.0"

cat << EOF
!
! VPN PROFILE ${VPC_NUM}
!
vrf definition AWS-${VPC_NUM} 
 rd 65533:${CSP_AWS}${VPC_NUM}
 !
 address-family ipv4
 exit-address-family
 exit

vrf definition GCP-${VPC_NUM} 
 rd 65533:${CSP_GCP}${VPC_NUM}
 !
 address-family ipv4
 exit-address-family
 exit

router bgp 65533
 address-family ipv4 vrf AWS-${VPC_NUM}
  neighbor AWS-${VPC_NUM} peer-group
  neighbor AWS-${VPC_NUM} description AWS Research VPC${VPC_NUM}
  neighbor AWS-${VPC_NUM} transport connection-mode passive
  neighbor AWS-${VPC_NUM} remote-as 65000
  neighbor AWS-${VPC_NUM} timers 10 30 30
  neighbor AWS-${VPC_NUM} soft-reconfiguration inbound
  network ${VPC_NET} mask 255.255.254.0
  maximum-paths 32
  exit
 !
 address-family ipv4 vrf GCP-${VPC_NUM}
  neighbor GCP-${VPC_NUM} peer-group
  neighbor GCP-${VPC_NUM} description GCP Research Project${VPC_NUM}
  neighbor GCP-${VPC_NUM} transport connection-mode passive
  neighbor GCP-${VPC_NUM} remote-as 65000
  neighbor GCP-${VPC_NUM} timers 20 60 60
  neighbor GCP-${VPC_NUM} soft-reconfiguration inbound
  network ${VPC_NET} mask 255.255.254.0
  maximum-paths 32
  exit
 exit

interface Loopback${CSP_AWS}${VPC_NUM}
 vrf forwarding AWS-${VPC_NUM}
 ip address 10.${VPC_NET_OCT2}.${VPC_NET_OCT3}.10 255.255.254.0
 exit

interface Loopback${CSP_GCP}${VPC_NUM}
 vrf forwarding GCP-${VPC_NUM}
 ip address 10.${VPC_NET_OCT2}.${VPC_NET_OCT3}.10 255.255.254.0
 exit

EOF

  for TUN_NUM in 1 2; do
    if [ ${TUN_NUM} -eq 1 ]; then
      CSP_VPN_ENDPOINT_AWS_A=${CSP_VPN_ENDPOINT_AWS1A}${EMORY_VPN_IP_OCT4}
      CSP_VPN_ENDPOINT_AWS_B=${CSP_VPN_ENDPOINT_AWS1B}${EMORY_VPN_IP_OCT4}

      CSP_VPN_ENDPOINT_GCP_A=${CSP_VPN_ENDPOINT_GCP1A}${EMORY_VPN_IP_OCT4}
      CSP_VPN_ENDPOINT_GCP_B=${CSP_VPN_ENDPOINT_GCP1B}${EMORY_VPN_IP_OCT4}

      CSP_VPN_ENDPOINT_MS_A=${CSP_VPN_ENDPOINT_MS1A}${EMORY_VPN_IP_OCT4}
      CSP_VPN_ENDPOINT_MS_B=${CSP_VPN_ENDPOINT_MS1B}${EMORY_VPN_IP_OCT4}

      TUN_INT_PREFIXA=10
      TUN_INT_PREFIXB=11
      TUN_IP_A=${TUN1A_IP}
      TUN_IP_B=${TUN1B_IP}
      E_TUN_IP_A=${E_TUN1A_IP}
      E_TUN_IP_B=${E_TUN1B_IP}
      EMORY_VPN_IP="172.16.76.${EMORY_VPN_IP_OCT4}"
      #echo "!(AWS1)"
    elif [ ${TUN_NUM} -eq 2 ]; then
      CSP_VPN_ENDPOINT_AWS_A=${CSP_VPN_ENDPOINT_AWS2A}${EMORY_VPN_IP_OCT4}
      CSP_VPN_ENDPOINT_AWS_B=${CSP_VPN_ENDPOINT_AWS2B}${EMORY_VPN_IP_OCT4}

      CSP_VPN_ENDPOINT_GCP_A=${CSP_VPN_ENDPOINT_GCP2A}${EMORY_VPN_IP_OCT4}
      CSP_VPN_ENDPOINT_GCP_B=${CSP_VPN_ENDPOINT_GCP2B}${EMORY_VPN_IP_OCT4}

      CSP_VPN_ENDPOINT_MS_A=${CSP_VPN_ENDPOINT_MS2A}${EMORY_VPN_IP_OCT4}
      CSP_VPN_ENDPOINT_MS_B=${CSP_VPN_ENDPOINT_MS2B}${EMORY_VPN_IP_OCT4}
      TUN_INT_PREFIXA=20
      TUN_INT_PREFIXB=21
      TUN_IP_A=${TUN2A_IP}
      TUN_IP_B=${TUN2B_IP}
      E_TUN_IP_A=${E_TUN2A_IP}
      E_TUN_IP_B=${E_TUN2B_IP}
      EMORY_VPN_IP="172.16.77.${EMORY_VPN_IP_OCT4}"
      #echo "!(AWS2)"
    fi

#
# Template output starts here
#
cat << EOF
!
! AWS VPC ${VPC_NUM} Tunnel ${TUN_NUM}A
!
interface Loopback${CSP_AWS}${TUN_INT_PREFIXA}${VPC_NUM}
 description AWS ${VPC_NUM} Tunnel${TUN_NUM}A VPN Endpoint
 vrf forwarding internet-vrf
 ip address ${CSP_VPN_ENDPOINT_AWS_A} 255.255.255.255
exit

crypto keyring keyring-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A vrf internet-vrf
  description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}A
  local-address ${CSP_VPN_ENDPOINT_AWS_A} internet-vrf
  pre-shared-key address ${EMORY_VPN_IP} key AWS-test${VPC_NUM}-${TUN_NUM}a
exit

crypto isakmp profile isakmp-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A
  description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}A
  keyring keyring-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A
  match identity address ${EMORY_VPN_IP} 255.255.255.255 internet-vrf
  local-address ${CSP_VPN_ENDPOINT_AWS_A} internet-vrf
exit
  
crypto ipsec transform-set ipsec-prop-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A esp-aes 256 esp-sha256-hmac 
  mode tunnel
exit

crypto ipsec profile ipsec-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A
  description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}A
  set security-association lifetime seconds 3600
  set transform-set ipsec-prop-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A
  set pfs group2
exit

interface Tunnel${CSP_AWS}${TUN_INT_PREFIXA}${VPC_NUM}
 description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}A (vpc-${VPC_NUM})
 vrf forwarding AWS-${VPC_NUM}
 ip address ${TUN_IP_A} 255.255.255.252
 ip tcp adjust-mss 1387
 tunnel source ${CSP_VPN_ENDPOINT_AWS_A}
 tunnel mode ipsec ipv4
 tunnel destination ${EMORY_VPN_IP}
 tunnel vrf internet-vrf
 tunnel protection ipsec profile ipsec-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}A
 ip virtual-reassembly
exit

router bgp 65533
 !bgp listen range ${E_TUN_IP_A}/32 peer-group AWS-${VPC_NUM}
 address-family ipv4 vrf AWS-${VPC_NUM}
  neighbor ${E_TUN_IP_A} peer-group AWS-${VPC_NUM}
  neighbor ${E_TUN_IP_A} activate
  exit
 exit

!
! GCP Project ${VPC_NUM} Tunnel ${TUN_NUM}A
!
interface Loopback${CSP_GCP}${TUN_INT_PREFIXA}${VPC_NUM}
 description GCP ${VPC_NUM} Tunnel${TUN_NUM}A VPN Endpoint
 vrf forwarding internet-vrf
 ip address ${CSP_VPN_ENDPOINT_GCP_A} 255.255.255.255
exit

crypto keyring keyring-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A vrf internet-vrf
  description GCP Research Project${VPC_NUM} Tunnel${TUN_NUM}A
  local-address ${CSP_VPN_ENDPOINT_GCP_A} internet-vrf
  pre-shared-key address ${EMORY_VPN_IP} key GCP-test${VPC_NUM}-${TUN_NUM}a
exit

crypto isakmp profile isakmp-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A
  description GCP Research Project${VPC_NUM} Tunnel${TUN_NUM}A
  keyring keyring-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A
  match identity address ${EMORY_VPN_IP} 255.255.255.255 internet-vrf
  local-address ${CSP_VPN_ENDPOINT_GCP_A} internet-vrf
exit
  
crypto ipsec transform-set ipsec-prop-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A esp-aes esp-sha-hmac
  mode tunnel
exit

crypto ipsec profile ipsec-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A
  description GCP Research Project${VPC_NUM} Tunnel${TUN_NUM}A
  set security-association lifetime kilobytes disable
  set security-association lifetime seconds 10800
  set transform-set ipsec-prop-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A
  set pfs group2
exit

interface Tunnel${CSP_GCP}${TUN_INT_PREFIXA}${VPC_NUM}
 description GCP Research Project${VPC_NUM} Tunnel${TUN_NUM}A (vpc-${VPC_NUM})
 vrf forwarding GCP-${VPC_NUM}
 ip address ${TUN_IP_A} 255.255.255.252
 ip mtu 1400
 ip tcp adjust-mss 1360
 tunnel source ${CSP_VPN_ENDPOINT_GCP_A}
 tunnel mode ipsec ipv4
 tunnel destination ${EMORY_VPN_IP}
 tunnel vrf internet-vrf
 tunnel protection ipsec profile ipsec-vpn-GCP-vpc${VPC_NUM}-tun${TUN_NUM}A
 ip virtual-reassembly
exit

router bgp 65533
 !bgp listen range ${E_TUN_IP_A}/32 peer-group GCP-${VPC_NUM}
 address-family ipv4 vrf GCP-${VPC_NUM}
  neighbor ${E_TUN_IP_A} peer-group GCP-${VPC_NUM}
  neighbor ${E_TUN_IP_A} activate
  exit
 exit

!
! VPC ${VPC_NUM} Tunnel ${TUN_NUM}B
!
interface Loopback${CSP_AWS}${TUN_INT_PREFIXB}${VPC_NUM}
 description AWS ${VPC_NUM} Tunnel${TUN_NUM}B VPN Endpoint
 vrf forwarding internet-vrf
 ip address ${CSP_VPN_ENDPOINT_AWS_B} 255.255.255.255
exit

crypto keyring keyring-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B vrf internet-vrf
  description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}B
  local-address ${CSP_VPN_ENDPOINT_AWS_B} internet-vrf
  pre-shared-key address ${EMORY_VPN_IP} key AWS-test${VPC_NUM}-${TUN_NUM}b
exit

crypto isakmp profile isakmp-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B
  description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}B
  keyring keyring-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B
  match identity address ${EMORY_VPN_IP} 255.255.255.255 internet-vrf
  local-address ${CSP_VPN_ENDPOINT_AWS_B} internet-vrf
exit
  
crypto ipsec transform-set ipsec-prop-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B esp-aes 256 esp-sha256-hmac 
  mode tunnel
exit

crypto ipsec profile ipsec-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B
  description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}B
  set security-association lifetime seconds 3600
  set transform-set ipsec-prop-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B
  set pfs group2
exit

interface Tunnel${CSP_AWS}${TUN_INT_PREFIXB}${VPC_NUM}
 description AWS Research VPC${VPC_NUM} Tunnel${TUN_NUM}B (vpc-${VPC_NUM})
 vrf forwarding AWS-${VPC_NUM}
 ip address ${TUN_IP_B} 255.255.255.252
 ip tcp adjust-mss 1387
 tunnel source ${CSP_VPN_ENDPOINT_AWS_B}
 tunnel mode ipsec ipv4
 tunnel destination ${EMORY_VPN_IP}
 tunnel vrf internet-vrf
 tunnel protection ipsec profile ipsec-vpn-AWS-vpc${VPC_NUM}-tun${TUN_NUM}B
 ip virtual-reassembly
exit

router bgp 65533
 !bgp listen range ${E_TUN_IP_B}/32 peer-group AWS-${VPC_NUM}
 address-family ipv4 vrf AWS-${VPC_NUM}
  neighbor ${E_TUN_IP_B} peer-group AWS-${VPC_NUM}
  neighbor ${E_TUN_IP_B} activate
  exit
 exit

EOF
#
# Template output ends here
#
  done
  TUN_IP_OCT4=$((${TUN_IP_OCT4}+4))
  VPC_NET_OCT3=$((${VPC_NET_OCT3}+2))
  EMORY_VPN_IP_OCT4=$((${EMORY_VPN_IP_OCT4}+1))
done

