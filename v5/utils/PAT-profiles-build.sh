#206.57.74.128/26
#10.65.0.0/23
let PUB_OCT4=128
let OCT4=0
let OCT3=0
let OCT2=65

for i in `seq 0 63`; do
  echo "! NAT Config For 10.${OCT2}.${OCT3}.0/21 global 206.57.74.${PUB_OCT4}"
  echo "object-group network NAT-Cloud-VPC-block-${OCT4}-objgrp "
  echo " 10.${OCT2}.${OCT3}.0 255.255.248.0"
  echo "!"
  echo "ip access-list extended NAT-Cloud-VPC-block-${OCT4}-acl"
  echo " permit ip object-group NAT-Cloud-VPC-block-${OCT4}-objgrp any"
  echo "!"
  echo "ip nat pool NAT-pool-206-57-74-${PUB_OCT4} 206.57.74.${PUB_OCT4} 206.57.74.${PUB_OCT4} netmask 255.255.255.255"
  echo "!"
  echo "ip nat inside source list NAT-Cloud-VPC-block-${OCT4}-acl pool NAT-pool-206-57-74-${PUB_OCT4} redundancy 1 mapping-id 100${OCT4} vrf NAT overload"
  echo "!"
  echo
  
  OCT4=$(($OCT4+1))
  PUB_OCT4=$(($PUB_OCT4+1))
  OCT3=$(($OCT3+8))

  if [ ${OCT3} -gt 248 ]; then
    OCT2=$(($OCT2+1))
    OCT3=0
  fi
done
