#!/bin/bash

#
# Recursively change VRF in templates from "Cloud" to something different
#

# argument ${1} is the new VRF name

# the RPC-Template directory must be in your local execution directory

if [ -n "${1}" ]; then
  if [ -d "RPC-Templates" ]; then

    sed -i \
    -e s/"<ios-bgp:name>Cloud<\/ios-bgp:name>"/"<ios-bgp:name>${1}<\/ios-bgp:name>"/g \
    -e s/"<ios-crypto:vrf>Cloud<\/ios-crypto:vrf>"/"<ios-crypto:vrf>${1}<\/ios-crypto:vrf>"/g \
    -e s/"<vrf-name>Cloud<\/vrf-name>"/"<vrf-name>${1}<\/vrf-name>"/g \
    `find ./RPC-Templates/ -type f`

  else
    echo "Can't find the RPC-Templates directory."
  fi
else
  echo "You must specify a new VRF name."
fi
