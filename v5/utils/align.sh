sed -i \
-e s/FIXME_TunnelNum[01]FIXME_VpcNum/'${TunnelId}'/g \
-e s/FIXME_VpcNum/'${VpnId-Padded}'/g \
-e s/FIXME_TunnelNum/'${TunnelNumber}'/g \
-e s/FIXME_VpcId/'${VpcId}'/g \
-e s/FIXME_RemoteVpnIpAddress[AB]/'${RemoteVpnIpAddress}'/g \
-e s/FIXME_LocalVpnIpAddress[AB]/'${LocalVpnIpAddress}'/g \
-e s/FIXME_PresharedKey[AB]/'${PresharedKey}'/g \
-e s/FIXME_BgpNeighborId[AB]/'${BgpNeighborId}'/g \
-e s/FIXME_MyBgpAsn/'${MyBgpAsn}'/g \
-e s/FIXME_GlobalIpAddress/'${GlobalIpAddress}'/g \
-e s/FIXME_LocalIpAddress/'${LocalIpAddress}'/g \
${1}
