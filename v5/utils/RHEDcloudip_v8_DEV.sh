#!/bin/bash

let x=1

for i in `seq 65 66`; do
  for j in `seq 0 2 254`; do
    VPC_NET[$x]="10.${i}.${j}.0/23"
    x=$((x+1))
  done
done

let x=1
let oct3A=248
let oct3B=240
let oct4=0
while [ $x -le 200 ]; do

  VPN_CIDR1A[$x]="169.254.${oct3A}.${oct4}/30"
  VPN_CIDR1B[$x]="169.254.${oct3B}.${oct4}/30"

  x=$((x+1))
  oct4=$((oct4+4))

  if [ $oct4 -gt 252 ]; then
    oct3A=$((oct3A+1))
    oct3B=$((oct3B+1))
    oct4=0
  fi
done

let x=1
let oct3A=252
let oct3B=244
let oct4=0
while [ $x -le 200 ]; do

  VPN_CIDR2A[$x]="169.254.${oct3A}.${oct4}/30"
  VPN_CIDR2B[$x]="169.254.${oct3B}.${oct4}/30"

  x=$((x+1))
  oct4=$((oct4+4))

  if [ $oct4 -gt 252 ]; then
    oct3A=$((oct3A+1))
    oct3B=$((oct3B+1))
    oct4=0
  fi
done

#let x=1
#let y=0

#for i in `seq 65 66`; do
  #for j in `seq 0 8 248`; do
    #NATL[$x]="10.${i}.${j}.0/21"
    #NATL[$((x+1))]="10.${i}.${j}.0/21"
    #NATL[$((x+2))]="10.${i}.${j}.0/21"
    #NATL[$((x+3))]="10.${i}.${j}.0/21"
#
    #NATG[$x]="170.140.143.${y}"
    #NATG[$((x+1))]="170.140.143.${y}"
    #NATG[$((x+2))]="170.140.143.${y}"
    #NATG[$((x+3))]="170.140.143.${y}"
    #x=$((x+4))
    #y=$((y+1))
  #done
#done

let j=1
E="DEV"
#echo VpnConnectionProfileId,VpcCidr,CustomerGatewayIpAddress_1,VpnInsideIpCidr_1A,VpnInsideIpCidr_1B,CustomerGatewayIpAddress_2,VpnInsideIpCidr_2A,VpnInsideIpCidr_2B,Environment
echo VpnConnectionProfileId,VpcCidr,CustomerGatewayIpAddress_1,VpnInsideIpCidr_1A,VpnInsideIpCidr_1B,TUNNEL_DESCRIPTION_1,CRYPTO_KEYRING_1,ISAKMP_PROFILE_1,IPSEC_TRANSFORM_SET_1,IPSEC_PROFILE_1,CustomerGatewayIpAddress_2,VpnInsideIpCidr_2A,VpnInsideIpCidr_2B,TUNNEL_DESCRIPTION_2,CRYPTO_KEYRING_2,ISAKMP_PROFILE_2,IPSEC_TRANSFORM_SET_2,IPSEC_PROFILE_2
while [ $j -le 200 ]; do
  VPC_NUM="`seq -w $j 200 200`"
  #CGW1_IP[$j]="170.140.76.${j}"
  #CGW2_IP[$j]="170.140.77.${j}"
  CGW1_IP[$j]="172.16.76.${j}"
  CGW2_IP[$j]="172.16.77.${j}"

  TUN_INT_1[$j]="10${VPC_NUM}"
  TUN_INT_2[$j]="20${VPC_NUM}"
  TUNNEL_DESCRIPTION_1="AWS Research VPC${VPC_NUM} Tunnel1"
  CRYPTO_KEYRING_1="keyring-vpn-research-vpc${VPC_NUM}-tun1"
  ISAKMP_PROFILE_1="isakmp-vpn-research-vpc${VPC_NUM}-tun1"
  IPSEC_TRANSFORM_SET_1="ipsec-prop-vpn-research-vpc${VPC_NUM}-tun1"
  IPSEC_PROFILE_1="ipsec-vpn-research-vpc${VPC_NUM}-tun1"

  TUNNEL_DESCRIPTION_2="AWS Research VPC${VPC_NUM} Tunnel2"
  CRYPTO_KEYRING_2="keyring-vpn-research-vpc${VPC_NUM}-tun2"
  ISAKMP_PROFILE_2="isakmp-vpn-research-vpc${VPC_NUM}-tun2"
  IPSEC_TRANSFORM_SET_2="ipsec-prop-vpn-research-vpc${VPC_NUM}-tun2"
  IPSEC_PROFILE_2="ipsec-vpn-research-vpc${VPC_NUM}-tun2"

  if [ $j -eq 21 ]; then
    E="QA"
  fi
  if [ $j -eq 61 ]; then
    E="PROD"
  fi
  #echo ${j},${VPC_NET[$j]},${CGW1_IP[$j]},${VPN_CIDR1A[$j]},${VPN_CIDR1B[$j]},${CGW2_IP[$j]},${VPN_CIDR2A[$j]},${VPN_CIDR2B[$j]},${E}
  echo ${j},${VPC_NET[$j]},${CGW1_IP[$j]},${VPN_CIDR1A[$j]},${VPN_CIDR1B[$j]},${TUNNEL_DESCRIPTION_1},${CRYPTO_KEYRING_1},${ISAKMP_PROFILE_1},${IPSEC_TRANSFORM_SET_1},${IPSEC_PROFILE_1},${CGW2_IP[$j]},${VPN_CIDR2A[$j]},${VPN_CIDR2B[$j]},${TUNNEL_DESCRIPTION_2},${CRYPTO_KEYRING_2},${ISAKMP_PROFILE_2},${IPSEC_TRANSFORM_SET_2},${IPSEC_PROFILE_2}
  j=$((j+1))
done
