#!/bin/bash

if [ ${1} -eq 1 ]; then
  VPN_NUM=1
elif [ ${1} -eq 2 ]; then
  VPN_NUM=2
else
  echo "You must specify a router number < 1 | 2 >"
  exit
fi

# APIPA addresses for tunnel transit 169.254.x.x
let TUN1A_IP_OCT3=248
let TUN1B_IP_OCT3=240
let TUN2A_IP_OCT3=252
let TUN2B_IP_OCT3=244
let TUN_IP_OCT4=1

# these are RFC1918 10.x.x.x/23
let VPC_NET_OCT2=65
let VPC_NET_OCT3=0

# should be public IP's unless in lab
#LOCAL_VPN_IP_BASE="172.16"
#let LOCAL_VPN_IP_OCT3_1=76
#let LOCAL_VPN_IP_OCT3_2=77
#let LOCAL_VPN_IP_OCT4=1

if [ ${VPN_NUM} -eq 1 ]; then
  LOCAL_VPN_IP="206.57.74.4"
  LONUM="10000"
elif [ ${VPN_NUM} -eq 2 ]; then
  LOCAL_VPN_IP="206.57.74.5"
  LONUM="20000"
fi

cat << EOF
! Only one VPN endpoint per router for RHEDcloud
interface Loopback${LONUM}
 description RHEDcloud VPN Tunnel Endpoint
 vrf forwarding Cloud
 ip address ${LOCAL_VPN_IP} 255.255.255.255
exit

EOF

for VPC_NUM in `seq -w 1 200`; do
  if [ ${TUN_IP_OCT4} -gt 254 ]; then
    TUN1A_IP_OCT3=$((${TUN1A_IP_OCT3}+1))
    TUN2A_IP_OCT3=$((${TUN2A_IP_OCT3}+1))
    TUN1B_IP_OCT3=$((${TUN1B_IP_OCT3}+1))
    TUN2B_IP_OCT3=$((${TUN2B_IP_OCT3}+1))
    TUN_IP_OCT4=1
  fi
  TUN1A_IP="169.254.${TUN1A_IP_OCT3}.${TUN_IP_OCT4}"
  TUN2A_IP="169.254.${TUN2A_IP_OCT3}.${TUN_IP_OCT4}"
  TUN1B_IP="169.254.${TUN1B_IP_OCT3}.${TUN_IP_OCT4}"
  TUN2B_IP="169.254.${TUN2B_IP_OCT3}.${TUN_IP_OCT4}"
  LOCAL_TUN1A_IP="169.254.${TUN1A_IP_OCT3}.$((${TUN_IP_OCT4}+1))"
  LOCAL_TUN2A_IP="169.254.${TUN2A_IP_OCT3}.$((${TUN_IP_OCT4}+1))"
  LOCAL_TUN1B_IP="169.254.${TUN1B_IP_OCT3}.$((${TUN_IP_OCT4}+1))"
  LOCAL_TUN2B_IP="169.254.${TUN2B_IP_OCT3}.$((${TUN_IP_OCT4}+1))"

  if [ ${VPC_NET_OCT3} -gt 254 ]; then
    VPC_NET_OCT2=$((${VPC_NET_OCT2}+1))
    VPC_NET_OCT3=0
  fi
  VPC_NET="10.${VPC_NET_OCT2}.${VPC_NET_OCT3}.0"

  if [ ${VPN_NUM} -eq 1 ]; then
    TUN_INT_PREFIXA=10
    TUN_INT_PREFIXB=11
    TUN_IP_A=${TUN1A_IP}
    TUN_IP_B=${TUN1B_IP}
    LOCAL_TUN_IP_A=${LOCAL_TUN1A_IP}
    LOCAL_TUN_IP_B=${LOCAL_TUN1B_IP}
    #LOCAL_VPN_IP="${LOCAL_VPN_IP_BASE}.${LOCAL_VPN_IP_OCT3_1}.${LOCAL_VPN_IP_OCT4}"
  elif [ ${VPN_NUM} -eq 2 ]; then
    TUN_INT_PREFIXA=20
    TUN_INT_PREFIXB=21
    TUN_IP_A=${TUN2A_IP}
    TUN_IP_B=${TUN2B_IP}
    LOCAL_TUN_IP_A=${LOCAL_TUN2A_IP}
    LOCAL_TUN_IP_B=${LOCAL_TUN2B_IP}
    #LOCAL_VPN_IP="${LOCAL_VPN_IP_BASE}.${LOCAL_VPN_IP_OCT3_2}.${LOCAL_VPN_IP_OCT4}"
  fi

#
# Template output starts here
#
cat << EOF
!
! VPN Profile ${VPC_NUM}
!
ip prefix-list AWS_RESEARCH_VPC_${VPC_NUM} seq 5 permit ${VPC_NET}/23
ip prefix-list AWS_RESEARCH_VPC_${VPC_NUM}_NEXT_HOP seq 5 permit ${TUN_IP_A}/32
ip prefix-list AWS_RESEARCH_VPC_${VPC_NUM}_NEXT_HOP seq 10 permit ${TUN_IP_B}/32

ip policy-list AWS_RESEARCH_VPC_${VPC_NUM}_NEXT_HOP permit
 match ip route-source prefix-list AWS_RESEARCH_VPC_${VPC_NUM}_NEXT_HOP
exit
route-map FROM_AWS_RESEARCH_VPCs permit ${VPN_NUM}0${VPC_NUM}
 match ip address prefix-list AWS_RESEARCH_VPC_${VPC_NUM}
 match policy-list AWS_RESEARCH_VPC_${VPC_NUM}_NEXT_HOP
exit
!
! VPN Profile ${VPC_NUM} Tunnel ${VPN_NUM}A
!
crypto keyring keyring-vpn-research-vpc${VPC_NUM}-tun${VPN_NUM} vrf Cloud
  local-address ${LOCAL_VPN_IP} Cloud
exit
crypto isakmp profile isakmp-vpn-research-vpc${VPC_NUM}-tun${VPN_NUM}
  keyring keyring-vpn-research-vpc${VPC_NUM}-tun${VPN_NUM}
  match identity address 169.254.0.1 255.255.255.255 Cloud
  local-address ${LOCAL_VPN_IP} Cloud
exit
interface Tunnel${TUN_INT_PREFIXA}${VPC_NUM}
 description AWS Research VPC${VPC_NUM} Tunnel${VPN_NUM}A (AVAILABLE)
 vrf forwarding Cloud
 ip address ${LOCAL_TUN_IP_A} 255.255.255.252
 ip tcp adjust-mss 1387
 shutdown
 tunnel source ${LOCAL_VPN_IP}
 tunnel mode ipsec ipv4
 tunnel vrf Cloud
 ip virtual-reassembly
exit
!
! VPN Profile ${VPC_NUM} Tunnel ${VPN_NUM}B
!
crypto keyring keyring-vpn-research-vpc${VPC_NUM}-tun${VPN_NUM}B vrf Cloud
  local-address ${LOCAL_VPN_IP} Cloud
exit
crypto isakmp profile isakmp-vpn-research-vpc${VPC_NUM}-tun${VPN_NUM}B
  keyring keyring-vpn-research-vpc${VPC_NUM}-tun${VPN_NUM}B
  match identity address 169.254.0.1 255.255.255.255 Cloud
  local-address ${LOCAL_VPN_IP} Cloud
exit
interface Tunnel${TUN_INT_PREFIXB}${VPC_NUM}
 description AWS Research VPC${VPC_NUM} Tunnel${VPN_NUM}B (AVAILABLE)
 vrf forwarding Cloud
 ip address ${LOCAL_TUN_IP_B} 255.255.255.252
 ip tcp adjust-mss 1387
 shutdown
 tunnel source ${LOCAL_VPN_IP}
 tunnel mode ipsec ipv4
 tunnel vrf Cloud
 ip virtual-reassembly
exit
EOF
  TUN_IP_OCT4=$((${TUN_IP_OCT4}+4))
  VPC_NET_OCT3=$((${VPC_NET_OCT3}+2))
  #LOCAL_VPN_IP_OCT4=$((${LOCAL_VPN_IP_OCT4}+1))
done

# static global content here
cat << EOF
!
! other static global config
!
crypto isakmp policy 9000
 encr aes 256
 hash sha256
 authentication pre-share
 group 2
 lifetime 28800
exit
crypto isakmp policy 9010
 encr aes
 authentication pre-share
 group 2
 lifetime 36600
exit
crypto isakmp keepalive 10 10
crypto ipsec security-association replay window-size 128
crypto ipsec df-bit clear

ip route vrf Cloud 10.0.0.0 255.0.0.0 Null0 254
ip route vrf Cloud 172.16.0.0 255.240.0.0 Null0 254
ip route vrf Cloud 192.168.0.0 255.255.0.0 Null0 254

ip prefix-list TO_AWS_RESEARCH_VPCs seq 30 permit 10.0.0.0/8
ip prefix-list TO_AWS_RESEARCH_VPCs seq 40 permit 172.16.0.0/12
ip prefix-list TO_AWS_RESEARCH_VPCs seq 50 permit 192.168.0.0/16
ip prefix-list TO_AWS_RESEARCH_VPCs seq 60 permit 0.0.0.0/0

route-map TO_AWS_RESEARCH_VPCs permit 10 
 match ip address prefix-list TO_AWS_RESEARCH_VPCs

router bgp 65000
 address-family ipv4 vrf Cloud
  network 0.0.0.0
  network 10.0.0.0
  network 172.16.0.0 mask 255.240.0.0
  network 192.168.0.0 mask 255.255.0.0
  neighbor AWS_RESEARCH_VPCs peer-group
  neighbor AWS_RESEARCH_VPCs remote-as 65533
  neighbor AWS_RESEARCH_VPCs description AWS Research VPCs via IPSEC VPN
  neighbor AWS_RESEARCH_VPCs timers 10 30 30
  neighbor AWS_RESEARCH_VPCs soft-reconfiguration inbound
  neighbor AWS_RESEARCH_VPCs route-map FROM_AWS_RESEARCH_VPCs in
  neighbor AWS_RESEARCH_VPCs route-map TO_AWS_RESEARCH_VPCs out
  neighbor GCP_RESEARCH_VPCs peer-group
  neighbor GCP_RESEARCH_VPCs remote-as 65533
  neighbor GCP_RESEARCH_VPCs description GCP Research VPCs via IPSEC VPN
  neighbor GCP_RESEARCH_VPCs timers 20 60 60
  neighbor GCP_RESEARCH_VPCs soft-reconfiguration inbound
  neighbor GCP_RESEARCH_VPCs route-map FROM_AWS_RESEARCH_VPCs in
  neighbor GCP_RESEARCH_VPCs route-map TO_AWS_RESEARCH_VPCs out
  maximum-paths 32
 exit-address-family
exit
EOF
