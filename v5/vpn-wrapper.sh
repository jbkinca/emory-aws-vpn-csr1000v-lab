#
#        Name: vpn-wrapper.sh
# Description: bash wrapper script for provisioning multiple VPNs in the virtual lab environment.
#              Calls rcvpnctl.py, but values to arguments are only valid in the lab.
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#

# Base args for all actions - add/delete/status
#
# $1 = Action to perform - <add | delete | status>
#
# $2 = First VPC in the loop - (integer >= 1)
#
# $3 = Last VPC in the loop - (integer <= 200, can be same as $2 if only touching a single VPC)
#
#
# Action specific args - add/delete
#
# $4 = CSP <AWS | GCP>
#
# $5 = Tunnels to act on <A | B | AB>
#
# $6 = Verbose <0 | 1> - (default 0)
#
# Action specific args - status
#
# $4 = Verbose <0 | 1> - (default 0)

# Examples
#
# Add VPN config for profiles 1-5 for AWS with tunnels A & B (print verbose info)
# ./vpn-wrapper.sh add 1 5 AWS AB 1
#
# Show status (intent & state) for profiles 1-5
# ./vpn-wrapper.sh status 1 5
#
# Delete VPN config for profiles 1-5 for AWS for tunnel B
# ./vpn-wrapper.sh delete 1 5 AWS B
#
# Delete VPN config for profiles 1-5 for AWS for tunnel A
# ./vpn-wrapper.sh delete 1 5 AWS A
#
# Add VPN config for profile 1 for GCP for tunnel A
# ./vpn-wrapper.sh add 1 1 GCP A
#
# Show status (intent & state) for profile 1 (print verbose info)
# ./vpn-wrapper.sh status 1 1 1
#
# Delete VPN config for profile 1 for GCP for tunnel A (print verbose info)
# ./vpn-wrapper.sh delete 1 1 GCP A 1
#

#
# Edit these to match your lab setup
#
Router1="r1"
Router2="r2"
user="admin"
password="cisco"

# Local on-prem side VPN endpoints - 2 x /16's
# RFC1918 in the lab, but real world is public
LocalVpnIpBase1="172.16.76"
LocalVpnIpBase2="172.16.77"

# CSP side VPN endpoints - 2 x /16's
# RFC1918 in the lab, but real world is public
RemoteVpnIpBase1="10.1"
RemoteVpnIpBase2="10.2"

# BgpNeighborId is VpnInsideCidr + 1
# Each tunnel requires a /30
# That's 200 x 4 tunnels or 800 /30's needed
# APIPA addresses are used in real world and required by some CSP's
# Can't change Oct1, 2, or 4 (169.254.X.0)
# For Tunnel 1A, BgpNeighborId's start at 169.254.${BgpNeighborId1A_Oct3}.0
let BgpNeighborId1A_Oct3=248
# For Tunnel 1B, BgpNeighborId's start at 169.254.${BgpNeighborId1B_Oct3}.0
let BgpNeighborId1B_Oct3=240
# For Tunnel 2A, BgpNeighborId's start at 169.254.${BgpNeighborId2A_Oct3}.0
let BgpNeighborId2A_Oct3=252
# For Tunnel 2B, BgpNeighborId's start at 169.254.${BgpNeighborId2B_Oct3}.0
let BgpNeighborId2B_Oct3=244

#
# Don't edit below this line
#
CSP_AWS=50
CSP_GCP=49
CSP_MS=48

for i in `seq $2 $3`; do
  if [ ${1} == "add" ] || [ ${1} == "delete" ]; then

    VpcId="vpc-`printf "%03d\n" ${i}`"
    LocalVpnIpAddress1A=${LocalVpnIpBase1}.${i}
    LocalVpnIpAddress1B=${LocalVpnIpBase1}.${i}
    LocalVpnIpAddress2A=${LocalVpnIpBase2}.${i}
    LocalVpnIpAddress2B=${LocalVpnIpBase2}.${i}

    if [ ${4} == "AWS" ]; then
      RemoteVpnIpAddress1A=${RemoteVpnIpBase1}${CSP_AWS}.0.${i}
      RemoteVpnIpAddress1B=${RemoteVpnIpBase1}${CSP_AWS}.1.${i}
      RemoteVpnIpAddress2A=${RemoteVpnIpBase2}${CSP_AWS}.0.${i}
      RemoteVpnIpAddress2B=${RemoteVpnIpBase2}${CSP_AWS}.1.${i}
      PresharedKey1A="AWS-test`printf "%03d\n" ${i}`-1a"
      PresharedKey1B="AWS-test`printf "%03d\n" ${i}`-1b"
      PresharedKey2A="AWS-test`printf "%03d\n" ${i}`-2a"
      PresharedKey2B="AWS-test`printf "%03d\n" ${i}`-2b"
    elif [ ${4} == "GCP" ]; then
      RemoteVpnIpAddress1A=${RemoteVpnIpBase1}${CSP_GCP}.0.${i}
      RemoteVpnIpAddress1B=${RemoteVpnIpBase1}${CSP_GCP}.1.${i}
      RemoteVpnIpAddress2A=${RemoteVpnIpBase2}${CSP_GCP}.0.${i}
      RemoteVpnIpAddress2B=${RemoteVpnIpBase2}${CSP_GCP}.1.${i}
      PresharedKey1A="GCP-test`printf "%03d\n" ${i}`-1a"
      PresharedKey1B="GCP-test`printf "%03d\n" ${i}`-1b"
      PresharedKey2A="GCP-test`printf "%03d\n" ${i}`-2a"
      PresharedKey2B="GCP-test`printf "%03d\n" ${i}`-2b"
    else
      echo "Unsupported CSP specified [${4}]"
      exit
    fi

    #
    # Calculate BgpNeighborId - CSP gets first IP, local site gets second IP
    #
    let BgpNeighborId_Oct4=-4

    for j in `seq 1 ${i}`; do
      BgpNeighborId_Oct4=$((${BgpNeighborId_Oct4} + 4))

      if [ ${BgpNeighborId_Oct4} -eq 256 ]; then
        BgpNeighborId_Oct4=0
        BgpNeighborId1A_Oct3=$((${BgpNeighborId1A_Oct3} + 1))
        BgpNeighborId1B_Oct3=$((${BgpNeighborId1B_Oct3} + 1))
        BgpNeighborId2A_Oct3=$((${BgpNeighborId2A_Oct3} + 1))
        BgpNeighborId2B_Oct3=$((${BgpNeighborId2B_Oct3} + 1))
      fi
    done

    # add 1 to get the neighbor IP
    BgpNeighborId_Oct4=$((${BgpNeighborId_Oct4} + 1))

    # produce the neighbors
    BgpNeighborId1A="169.254.${BgpNeighborId1A_Oct3}.${BgpNeighborId_Oct4}"
    BgpNeighborId1B="169.254.${BgpNeighborId1B_Oct3}.${BgpNeighborId_Oct4}"
    BgpNeighborId2A="169.254.${BgpNeighborId2A_Oct3}.${BgpNeighborId_Oct4}"
    BgpNeighborId2B="169.254.${BgpNeighborId2B_Oct3}.${BgpNeighborId_Oct4}"

    if [ "$6" == "1" ]; then
      VERBOSE="--verbose 1"
    else
      VERBOSE=""
    fi

    if [ ${5} == "AB" ]; then
      TUNNEL_OPS_1="--LocalVpnIpAddressA ${LocalVpnIpAddress1A} --RemoteVpnIpAddressA ${RemoteVpnIpAddress1A} --PresharedKeyA ${PresharedKey1A} --BgpNeighborIdA ${BgpNeighborId1A} --LocalVpnIpAddressB ${LocalVpnIpAddress1B} --RemoteVpnIpAddressB ${RemoteVpnIpAddress1B} --PresharedKeyB ${PresharedKey1B} --BgpNeighborIdB ${BgpNeighborId1B}"
      TUNNEL_OPS_2="--LocalVpnIpAddressA ${LocalVpnIpAddress2A} --RemoteVpnIpAddressA ${RemoteVpnIpAddress2A} --PresharedKeyA ${PresharedKey2A} --BgpNeighborIdA ${BgpNeighborId2A} --LocalVpnIpAddressB ${LocalVpnIpAddress2B} --RemoteVpnIpAddressB ${RemoteVpnIpAddress2B} --PresharedKeyB ${PresharedKey2B} --BgpNeighborIdB ${BgpNeighborId2B}"
    elif [ ${5} == "A" ]; then
      TUNNEL_OPS_1="--LocalVpnIpAddressA ${LocalVpnIpAddress1A} --RemoteVpnIpAddressA ${RemoteVpnIpAddress1A} --PresharedKeyA ${PresharedKey1A} --BgpNeighborIdA ${BgpNeighborId1A}"
      TUNNEL_OPS_2="--LocalVpnIpAddressA ${LocalVpnIpAddress2A} --RemoteVpnIpAddressA ${RemoteVpnIpAddress2A} --PresharedKeyA ${PresharedKey2A} --BgpNeighborIdA ${BgpNeighborId2A}"
    elif [ ${5} == "B" ]; then
      TUNNEL_OPS_1="--LocalVpnIpAddressB ${LocalVpnIpAddress1B} --RemoteVpnIpAddressB ${RemoteVpnIpAddress1B} --PresharedKeyB ${PresharedKey1B} --BgpNeighborIdB ${BgpNeighborId1B}"
      TUNNEL_OPS_2="--LocalVpnIpAddressB ${LocalVpnIpAddress2B} --RemoteVpnIpAddressB ${RemoteVpnIpAddress2B} --PresharedKeyB ${PresharedKey2B} --BgpNeighborIdB ${BgpNeighborId2B}"
    fi

    # add/delete
    echo "*** VPN Profile $i Router 1 Tunnel(s) ${5} - Action:  $1 ***"
    python rcvpnctl.py -a ${Router1} -u ${user} -p ${password} --device 1 --VpcNum ${i} ${VERBOSE} ${1} ${4} --VpcId ${VpcId} --Tunnels ${5} ${TUNNEL_OPS_1}

    echo "*** VPN Profile $i Router 2 Tunnel(s) ${5} - Action:  $1 ***"
    python rcvpnctl.py -a ${Router2} -u ${user} -p ${password} --device 2 --VpcNum ${i} ${VERBOSE} ${1} ${4} --VpcId ${VpcId} --Tunnels ${5} ${TUNNEL_OPS_2}

  elif [ ${1} == "status" ]; then
    if [ "$4" == "1" ]; then
      VERBOSE="--verbose 1"
    else
      VERBOSE=""
    fi

    # status
    echo "*** VPN Profile $i Router 1 - Action:  $1 ***"
    python rcvpnctl.py -a ${Router1} -u ${user} -p ${password} --device 1 --VpcNum ${i} ${VERBOSE} ${1} --detail brief

    echo "*** VPN Profile $i Router 2 - Action:  $1 ***"
    python rcvpnctl.py -a ${Router2} -u ${user} -p ${password} --device 2 --VpcNum ${i} ${VERBOSE} ${1} --detail brief
  fi
done

