#
#        Name: rcvpnctl.py
# Description: RHEDCloud VPN Control
#              Add/check/delete VPNs and static NAT entries via NETCONF/YANG
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#
from __future__ import print_function
import sys
import lxml.etree as ET
import re
import time
from ncclient.operations import RPCError

def NETCONF_Datastore(m, args):
    DATASTORE = "running"

    Caps = m.server_capabilities
    for c in m.server_capabilities:
        #
        # Check for candidate datastore support
        #
        #urn:ietf:params:netconf:capability:candidate:1.0
        s = re.search('urn:ietf:params:netconf:capability:candidate:1.0', c)
        if s:
            # candidate datastore detected on device
            DATASTORE = "candidate"

    if args.verbose > 0:
        print("DATASTORE:", DATASTORE, sep=' ')

    return DATASTORE

def YANG_Version(m, args):
    Caps = m.server_capabilities
    for c in m.server_capabilities:
        #
        # Retrieve relevant device model versions
        #
        #http://cisco.com/ns/yang/Cisco-IOS-XE-snmp?module=Cisco-IOS-XE-snmp&revision=2017-11-27
        s = re.search('http://cisco.com/ns/yang/Cisco-IOS-XE-.*\?module=(Cisco-IOS-XE-.*)\&revision=(.*)', c)
        if s:
            model_name = s.group(1)
            model_ver = s.group(2)
            if model_name == "Cisco-IOS-XE-tunnel":
                TunnelSchemaVer = model_ver
            elif model_name == "Cisco-IOS-XE-crypto":
                CryptoSchemaVer = model_ver
            elif model_name == "Cisco-IOS-XE-native":
                NativeSchemaVer = model_ver
            elif model_name == "Cisco-IOS-XE-bgp":
                BgpSchemaVer = model_ver
            elif model_name == "Cisco-IOS-XE-nat":
                NatSchemaVer = model_ver

    if args.verbose > 0:
        print("YANG model schema versions:")
        print("Cisco-IOS-XE-tunnel:", TunnelSchemaVer, sep=' ')
        print("Cisco-IOS-XE-crypto:", CryptoSchemaVer, sep=' ')
        print("Cisco-IOS-XE-native:", NativeSchemaVer, sep=' ')
        print("Cisco-IOS-XE-bgp:", BgpSchemaVer, sep=' ')
        print("Cisco-IOS-XE-nat:", NatSchemaVer, sep=' ')

    #
    # Determine YANG schema version
    #
    # See versions here: https://github.com/YangModels/yang/tree/master/vendor/cisco/xe
    #
    # Only supported YANG model versions are
    #     1662 i.e. 16.7.1 > "IOS-XE Version" >= 16.6.2
    #     1693 i.e. 16.10.1 > "IOS-XE Version" >= 16.9.3
    #
    # default to latest supported version
    # current is a symbolic link
    IOS_XE_YANG_VERSION = "current"

    #YANG model schema versions (1662):
    #Cisco-IOS-XE-tunnel: 2017-07-11
    #Cisco-IOS-XE-crypto: 2017-05-10
    #Cisco-IOS-XE-native: 2017-08-30
    #Cisco-IOS-XE-bgp: 2017-04-28
    #Cisco-IOS-XE-nat: 2017-08-03

    if TunnelSchemaVer == "2017-07-11" and CryptoSchemaVer == "2017-05-10" and NativeSchemaVer == "2017-08-30" and BgpSchemaVer == "2017-04-28" and NatSchemaVer == "2017-08-03":
        IOS_XE_YANG_VERSION = "1662"

    #YANG model schema versions (1693):
    #Cisco-IOS-XE-tunnel: 2017-08-28
    #Cisco-IOS-XE-crypto: 2019-04-25
    #Cisco-IOS-XE-native: 2018-07-27
    #Cisco-IOS-XE-bgp: 2019-01-09
    #Cisco-IOS-XE-nat: 2018-12-18

    if TunnelSchemaVer == "2017-08-28" and CryptoSchemaVer == "2019-04-25" and NativeSchemaVer == "2018-07-27" and BgpSchemaVer == "2019-01-09" and NatSchemaVer == "2018-12-18":
        IOS_XE_YANG_VERSION = "1693"

    # print verbose info
    if args.verbose > 0:
        print("IOS-XE YANG Version:", IOS_XE_YANG_VERSION, sep=' ')

    return IOS_XE_YANG_VERSION

def Load_RPC_Template(m, action, args, IOS_XE_YANG_VERSION, service, Csp):
    #
    # Load correct RPC templatebased on
    #     YANG version (1662 or 1693)
    #     action (add, delete, status, or shutdown)
    #     service (VPN, NAT, ASN, Tunnel) 
    #     CSP (AWS or GCP)

    RPC = ""

    #
    # Determine CSP
    #
    if service == "VPN":
        if action != "status":
            # add/delete requires Csp argument
            if args.Csp == "AWS":
                CspSuffix = "-AWS"
            elif args.Csp == "GCP":
                CspSuffix = "-GCP"

        elif action == "status":
            # status passes Csp argument as discovered from router
            if Csp == "AWS":
                CspSuffix = "-AWS"
            elif Csp == "GCP":
                CspSuffix = "-GCP"

        f = open("RPC-Templates/" + IOS_XE_YANG_VERSION + "/" + action + "/Tunnel" + CspSuffix,"r")

    if service == "NAT":
        f = open("RPC-Templates/" + IOS_XE_YANG_VERSION + "/" + action + "/NAT","r")

    if service == "ASN":
        f = open("RPC-Templates/" + IOS_XE_YANG_VERSION + "/util/ASN","r")

    if service == "Tunnel":
        f = open("RPC-Templates/" + IOS_XE_YANG_VERSION + "/util/Tunnel","r")

    if f.mode == 'r':
        RPC = f.read()
        f.close()
    else:
        sys.exit("ERROR - Unable to open RPC template file.")

    return RPC

def Finalize_RPC(action, RPC):
    #
    # Construct final RPC from the concatenated hydrated templates
    #
    CONFIG_HEADER = """<config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">"""
    CONFIG_FOOTER = """</config>"""
    GET_HEADER = """<filter>"""
    GET_FOOTER = """</filter>"""

    if action == "MODIFY":
        RPC_HEADER = CONFIG_HEADER
        RPC_FOOTER = CONFIG_FOOTER

    elif action == "QUERY":
        RPC_HEADER = GET_HEADER
        RPC_FOOTER = GET_FOOTER

    #
    # Build RPC
    #
    RPC = RPC_HEADER + RPC + RPC_FOOTER

    return RPC

def Lock_DS(m, ds, LockRetries, RetryCooldown):
    # Lock a datastore
    lt = 1
    RC1 = 0
    while lt <= LockRetries:
        if RC1 > 0:
            print("Sleeping for", str(RC1), "seconds until next retry.", sep=' ')
            time.sleep(RC1)
        try:
            RC2 = 0
            print("Attempting to lock", ds, "datastore - retry", str(lt), "of", str(LockRetries), "... ", sep=' ', end='')
            m.lock(ds)
            lt = LockRetries + 1

        except RPCError as e:
            #data = e._raw
            #print(ET.tostring(data, pretty_print=True))
            lt=lt+1
            RC1 = RetryCooldown
            print("FAIL.")
            print("WARNING - Unable to lock", ds, " datastore.", sep=' ')
            print(e.message)

        else:
            print("SUCCESS.")
            return

    print("ERROR - Failed to lock", ds, "datastore after", str(LockRetries), "attempts.", sep=' ')
    sys.exit()

def Discard_Config(m):
    try:
        print("Discarding candidate configuration.")
        m.discard_changes()

    except RPCError as e:
        print("RPC ERROR - Unable to discard candidate configuration.")
        #data = e._raw
        #print(ET.tostring(data, pretty_print=True))
        print(e.message)
        sys.exit()

    except Exception as e:
        print("ERROR - Unable to discard candidate configuration.")
        print(e.message)
        sys.exit()

    else:
        print("SUCCESS.")
        sys.exit()

def Commit_Config(m):
    try:
        print("Validating candidate configuration ... ", end='')
        response = m.validate("candidate")

    except RPCError as e:
        print("FAIL.")
        print("RPC ERROR - candidate configuration failed validation.")
        #data = e._raw
        #print(ET.tostring(data, pretty_print=True))
        print(e.message)
        Discard_Config(m)

    except Exception as e:
        print("FAIL.")
        print("ERROR - candidate configuration failed validation.")
        print(e.message)
        Discard_Config(m)

    else:
        print("SUCCESS.")
        try:
            print("Committing candidate configuration to running datastore ... ", end='')
            response = m.commit()

        except RPCError as e2:
            print("FAIL.")
            print("RPC ERROR - failed to commit candidate configuration to running datastore.")
            #data2 = e2._raw
            #print(ET.tostring(data2, pretty_print=True))
            print(e2.message)
            Discard_Config(m)

        except Exception as e2:
            print("FAIL.")
            print("ERROR - failed to commit candidate configuration to running datastore.")
            print(e2.message)
            Discard_Config(m)

        else:
            print("SUCCESS.")

def Edit_Config(m, ds, rpc, args):
    try:
        # Configure the VPN
        print("Editing the", ds, "configuration ... ", sep=' ', end='')
        if args.verbose > 0:
            print(rpc)

        response = m.edit_config(target=ds, error_option='rollback-on-error', config=rpc).xml

    except RPCError as e:
        #data = e._raw
        #print(ET.tostring(data, pretty_print=True))
        print("FAIL.")
        print("RPC ERROR - Unable to edit configuration.")
        if e.message:
            print(e.message)

        print(e.tag)
        sys.exit()

    except Exception as e:
        print("FAIL.")
        if e.message:
            print(e.message)
        sys.exit("ERROR - Unable to edit configuration.")

    else:
        print("SUCCESS.")
        #data = ET.fromstring(response)
        #print(ET.tostring(data, pretty_print=True))
        if ds == "candidate":
            Commit_Config(m)

        return

def Get_Util(m, args, IOS_XE_YANG_VERSION, RPC, util, TunnelId):
    # get BGP ASN or CSP from the router's configuration via NETCONF
    try:
        if args.verbose > 0:
            print(RPC)

        response = m.get(filter=RPC).xml
        root = ET.fromstring(response)

        if args.verbose > 0:
            print(ET.tostring(root, pretty_print=True))

        if util == "ASN":
            #/data/native/router/bgp/id
            #root[0][0][0][0][0].text

            #s = re.search('<id>(.*)</id>', response)
            s = root[0][0][0][0][0].text

            if s:
                #ASN = s.group(1)
                ASN = s

                # print verbose info
                if args.verbose > 0:
                    print("BGP ASN:", ASN, sep=' ')

                return ASN

        if util == "CSP" or util == "BgpNeighborId":
            #
            # Remove XML namespace prefixes
            # This is required in order to use ET.find() or ET.findall()
            #
            for elem in root.getiterator():
                elem.tag = ET.QName(elem).localname

            ET.cleanup_namespaces(root)

            # Uncomment to see XML post namespace removal
            #print(ET.tostring(root, pretty_print=True).decode())

            # select Tunnel interface configuration data
            TunnelIfConf = root.find("./data/native/interface/Tunnel[name='" + TunnelId + "']")
            #print(ET.tostring(TunnelIfConf, pretty_print=True))

            if (TunnelIfConf == None):
                #sys.exit("ERROR - unable to locate tunnel interface config.")
                # this can happen if Tunnel B is not pre-configured on the router
                # as is the case with the initial implementation that supported
                # only 1 Tunnel for AWS or if the CSP is GCP which supports only
                # 1 Tunnel.
                return None

            else:
                if util == "CSP":
                    # get ipsec profile name that is applied to tunnel interface
                    IpsecProfileName = TunnelIfConf.find("./tunnel/protection/ipsec/profile")

                    if IpsecProfileName == None:
                        return None

                    else:
                        #print(IpsecProfileName.text)
                        CSP1 = re.search('ipsec-vpn-(.*)-vpc.*', IpsecProfileName.text)
                        CSP2 = re.search('ipsec-vpn-(.*)-vpc.*', IpsecProfileName.text)
    
                        if CSP1.group(1) == "research":
                            CSP = "AWS"
                        elif CSP2.group(1) == "GCP":
                            CSP = "GCP"
                        else:
                            sys.exit("ERROR - unable to determine CSP from tunnel interface ipsec vpn profile config.")

                        # print verbose info
                        if args.verbose > 0:
                            print("CSP:", CSP, sep=' ')

                        return CSP

                if util == "BgpNeighborId":
                    # get tunnel ip address
                    MyTunnelIp = TunnelIfConf.find("./ip/address/primary/address")

                    if MyTunnelIp == None:
                        return None

                    else:
                        Oct = MyTunnelIp.text.split(".")
                        BgpNeighborId = Oct[0] + "." + Oct[1] + "." + Oct[2] + "." + str(int(Oct[3]) - 1)

                        if args.verbose > 0:
                            print("BgpNeighborId:", BgpNeighborId, sep=' ')

                        return BgpNeighborId

    except RPCError as e:
        #root = e._raw
        #print(ET.tostring(root, pretty_print=True))
        print(e.message)

    sys.exit("ERROR - Unable to determine local BGP ASN.")

def Print_Status(args, IOS_XE_YANG_VERSION, TunnelNum, BgpNeighborState, TunnelIfState, TunnelIfConf, CryptoConf, BgpConf):
    TunIsShut = True
    TunIsConfigured = False
    TunIsAdminUp = False
    TunIsOperUp = False
    BgpIsConfigured = False
    BgpNeiIsUp = False
    CryptoIsConfigured = False
    IfName = None

    # get tunnel config
    if TunnelIfConf != None:
        IfName = TunnelIfConf.find("./name")
        IfDesc = TunnelIfConf.find("./description")
        IfVrfFwd = TunnelIfConf.find("./vrf/forwarding")
        IfIpAddr = TunnelIfConf.find("./ip/address/primary/address")
        IfIpMask = TunnelIfConf.find("./ip/address/primary/mask")
        IfIpMtu = TunnelIfConf.find("./ip/mtu")
        IfIpTcpMss = TunnelIfConf.find("./ip/tcp/adjust-mss")
        IfShut = TunnelIfConf.find("./shutdown")
        IfTunSrc = TunnelIfConf.find("./tunnel/source")
        IfTunModeIpsec = TunnelIfConf.find("./tunnel/mode/ipsec/*")
        if(IOS_XE_YANG_VERSION == "1662"):
           IfTunDst = TunnelIfConf.find("./tunnel/destination")
        elif(IOS_XE_YANG_VERSION == "1693"):
           IfTunDst = TunnelIfConf.find("./tunnel/destination/ipaddress-or-host")
        IfTunVrf = TunnelIfConf.find("./tunnel/vrf")
        IfTunProtIpsecProf = TunnelIfConf.find("./tunnel/protection/ipsec/profile")

    # get tunnel state
    if TunnelIfState != None:
        IfStaName = TunnelIfState.find("./name")
        IfStaAdmin = TunnelIfState.find("./admin-status")
        IfStaOper = TunnelIfState.find("./oper-status")

    # get crypto config
    if CryptoConf != None:
        CryptoIsConfigured = True

        KeyringConf = CryptoConf.find("./keyring")
        IsakmpProfConf = CryptoConf.find("./isakmp/profile")
        TranSetConf = CryptoConf.find("./ipsec/transform-set")
        IpsecProfConf = CryptoConf.find("./ipsec/profile")

        if KeyringConf != None:
            KeyringName = KeyringConf.find("./name")
            KeyringVrf = KeyringConf.find("./vrf")
            KeyringDesc = KeyringConf.find("./description")
            KeyringPskIp = KeyringConf.find("./pre-shared-key/address/ipv4/ipv4-addr")
            KeyringPskKey = KeyringConf.find("./pre-shared-key/address/ipv4/key")
            KeyringPskUnKey = KeyringConf.find("./pre-shared-key/address/ipv4/unencryt-key")
            if IOS_XE_YANG_VERSION == "1693":
                # local-address not modeled in 1662 !!!
                KeyringLclAddrIp = KeyringConf.find("./local-address/bind-ip-address/ip-address")
                KeyringLclAddrVrf = KeyringConf.find("./local-address/bind-ip-address/vrf")

        if IsakmpProfConf != None:
            IsakmpProfName = IsakmpProfConf.find("./name")
            IsakmpProfDesc = IsakmpProfConf.find("./description")
            IsakmpProfVrf = IsakmpProfConf.find("./vrf")
            if IOS_XE_YANG_VERSION == "1662":
                IsakmpProfKeyring = IsakmpProfConf.find("./keyring")
                # local-address not modeled in 1662 !!!
            if IOS_XE_YANG_VERSION == "1693":
                IsakmpProfKeyring = IsakmpProfConf.find("./keyring/name")
                IsakmpProfLclAddrIp = IsakmpProfConf.find("./local-address/bind-ip-address/ip-address")
                IsakmpProfLclAddrVrf = IsakmpProfConf.find("./local-address/bind-ip-address/vrf")

        if TranSetConf != None:
            TranSetTag = TranSetConf.find("./tag")
            TranSetEsp = TranSetConf.find("./esp")
            TranSetKeyBit = TranSetConf.find("./key-bit")
            TranSetEspHmac = TranSetConf.find("./esp-hmac")
            TranSetMode = TranSetConf.find("./mode/*")

        if IpsecProfConf != None:
            IpsecProfName = IpsecProfConf.find("./name")
            IpsecProfDesc = IpsecProfConf.find("./description")
            IpsecProfTranSet = IpsecProfConf.find("./set/transform-set")
            IpsecProfPfsGrp = IpsecProfConf.find("./set/pfs/group")
            IpsecProfSaLifeKb = IpsecProfConf.find("./set/security-association/lifetime/kilobytes")
            IpsecProfSaLifeSec = IpsecProfConf.find("./set/security-association/lifetime/seconds")

    # get bgp config
    if BgpConf != None:
        BgpIsConfigured = True

        BgpId = BgpConf.find("./id")
        BgpAfIpv4VrfName = BgpConf.find("./address-family/with-vrf/ipv4/vrf/name")

        if IOS_XE_YANG_VERSION == "1662":
            BgpNeighborConf = BgpConf.find("./address-family/with-vrf/ipv4/vrf/neighbor")

        if IOS_XE_YANG_VERSION == "1693":
            BgpNeighborConf = BgpConf.find("./address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor")

        BgpPeerGroup = BgpNeighborConf.find("./peer-group/peer-group-name")
        BgpNeiId = BgpNeighborConf.find("./id")
        BgpNeiDesc = BgpNeighborConf.find("./description")
        BgpNeiAct = BgpNeighborConf.find("./activate")
        BgpNeiShutdown = BgpNeighborConf.find("./shutdown")

    # get bgp state
    if BgpNeighborState != None:
        BgpStaNeiId = BgpNeighborState.find("./neighbor-id")
        BgpStaDesc = BgpNeighborState.find('./description')
        BgpStaSess = BgpNeighborState.find('./session-state')
        if BgpStaSess != None:
            if BgpStaSess.text == "fsm-established":
                BgpNeiIsUp = True
                BgpStaUpTime = BgpNeighborState.find('./up-time')
                BgpStaPfxSent = BgpNeighborState.find('./prefix-activity/sent/current-prefixes')
                BgpStaPfxRcvd = BgpNeighborState.find('./prefix-activity/received/current-prefixes')
                BgpStaPfxInst = BgpNeighborState.find('./installed-prefixes')

    # print crypto config
    if CryptoConf != None:
        if args.detail == "all" or args.verbose > 0:
            print("! [CRYPTO CONFIG]")

            # keyring
            if KeyringName != None:
                print("crypto keyring", KeyringName.text, "vrf", KeyringVrf.text)
                if KeyringDesc != None:
                    print(" description", KeyringDesc.text)
                if IOS_XE_YANG_VERSION == "1693":
                    print(" local-address", KeyringLclAddrIp.text, KeyringLclAddrVrf.text)
                else:
                    if IfTunSrc != None and IfTunVrf != None:
                        print(" ! NOTE: \"local-address\" artificially synthesized (not modeled in 1662)")
                        print(" local-address", IfTunSrc.text, IfTunVrf.text)
                if KeyringPskIp != None and KeyringPskKey != None and KeyringPskUnKey != None:
                    print(" pre-shared-key address", KeyringPskIp.text, KeyringPskKey.tag, KeyringPskUnKey.text)

            # isakmp profile
            if IsakmpProfName != None:
                print("crypto isakmp profile", IsakmpProfName.text)
            if IsakmpProfDesc != None:
                print(" description", IsakmpProfDesc.text)
            if IsakmpProfVrf != None:
                print(" vrf", IsakmpProfVrf.text)
            if IsakmpProfKeyring != None:
                print(" keyring", IsakmpProfKeyring.text)

            if IOS_XE_YANG_VERSION == "1662":
                # loop through match identities
                # NOTE: 1662 has a bug and seems to only show most recently added identity
                # instead of showing the list
                for MatchId in IsakmpProfConf.findall("./match/identity/ipv4-address"):
                    matchidip = MatchId.find("./address")
                    matchidmask = MatchId.find("./mask")
                    matchidvrf = MatchId.find("./vrf")
                    if matchidip != None and matchidmask != None and matchidvrf != None:
                        print(" match identity address", matchidip.text, matchidmask.text, matchidvrf.text)

            if IOS_XE_YANG_VERSION == "1693":
                for MatchId in IsakmpProfConf.findall("./match/identity/address"):
                    matchidip = MatchId.find("./ip")
                    matchidmask = MatchId.find("./mask")
                    matchidvrf = MatchId.find("./vrf")
                    if matchidip != None and matchidmask != None and matchidvrf != None:
                        print(" match identity address", matchidip.text, matchidmask.text, matchidvrf.text)

            if IOS_XE_YANG_VERSION == "1693":
                print(" local-address", IsakmpProfLclAddrIp.text, IsakmpProfLclAddrVrf.text)
            else:
                if IfTunSrc != None and IfTunVrf != None:
                    print(" ! NOTE: \"local-address\" artificially synthesized (not modeled in 1662)")
                    print(" local-address", IfTunSrc.text, IfTunVrf.text)

            # transform set
            if TranSetTag != None and TranSetEsp != None and TranSetEspHmac != None:
                if TranSetKeyBit != None:
                    print("crypto ipsec transform-set", TranSetTag.text, TranSetEsp.text, TranSetKeyBit.text, TranSetEspHmac.text)
                else:
                    print("crypto ipsec transform-set", TranSetTag.text, TranSetEsp.text, TranSetEspHmac.text)
                if TranSetMode != None:
                    print(" mode", TranSetMode.tag)

            # ipsec profile
            if IpsecProfName != None:
                print("crypto ipsec profile", IpsecProfName.text)
                if IpsecProfDesc != None:
                    print(" description", IpsecProfDesc.text)
                if IpsecProfSaLifeKb != None:
                    print(" set security-association lifetime kilobytes", IpsecProfSaLifeKb.text)
                if IpsecProfSaLifeSec != None:
                    print(" set security-association lifetime seconds", IpsecProfSaLifeSec.text)
                if IpsecProfTranSet != None:
                    print(" set transform-set", IpsecProfTranSet.text)
                if IpsecProfPfsGrp != None:
                    print(" set pfs", IpsecProfPfsGrp.text)

            print("!\n")

    # print tunnel config
    if TunnelIfConf != None:
        if IfShut != None:
            TunIsShut = True
        else:
            TunIsShut = False

        if IfTunProtIpsecProf != None:
            TunIsConfigured = True

        if args.detail == "all" or args.verbose > 0:
            print("! [TUNNEL CONFIG]")
            if IfName != None:
                print("interface Tunnel", IfName.text, sep='')
            if IfDesc != None:
                print(" description", IfDesc.text)
            if IfVrfFwd != None:
                print(" vrf forwarding", IfVrfFwd.text)
            if IfIpAddr != None and IfIpMask != None:
                print(" ip address", IfIpAddr.text, IfIpMask.text)
            if IfIpMtu != None:
                print(" ip mtu", IfIpMtu.text)
            if IfIpTcpMss != None:
                print(" ip tcp adjust-mss", IfIpTcpMss.text)
            if IfShut != None:
                print(" ", IfShut.tag, sep='')
            if IfTunSrc != None:
                print(" tunnel source", IfTunSrc.text)
            if IfTunModeIpsec != None:
                print(" tunnel mode ipsec", IfTunModeIpsec.tag)
            if IfTunDst != None:
                print(" tunnel destination", IfTunDst.text)
            if IfTunVrf != None:
                print(" tunnel vrf", IfTunVrf.text)
            if IfTunProtIpsecProf != None:
                print(" tunnel protection ipsec profile", IfTunProtIpsecProf.text)

            print(" ! NOTE: \"ip virtual-reassembly\" added manually (not modeled in 1662)")
            print(" ip virtual-reassembly")
            print("!\n")

    # print bgp config
    if BgpConf != None:
        if args.detail == "all" or args.verbose > 0:
            print("! [BGP NEIGHBOR CONFIG]")

            if BgpId != None:
                print("router bgp", BgpId.text)

                if BgpAfIpv4VrfName != None:
                    print(" address-family ipv4 vrf", BgpAfIpv4VrfName.text)

                    if BgpNeiId != None:
                        if BgpPeerGroup != None:
                            print("  neighbor", BgpNeiId.text, "peer-group", BgpPeerGroup.text)
                        if BgpNeiDesc != None:
                            print("  neighbor", BgpNeiId.text, "description", BgpNeiDesc.text)
                        if BgpNeiShutdown != None:
                            print("  neighbor", BgpNeiId.text, BgpNeiShutdown.tag)
                        if BgpNeiAct != None:
                            print("  neighbor", BgpNeiId.text, BgpNeiAct.tag)

                    print(" exit address-family")
                print("!\n")

    # print tunnel state
    if TunnelIfState != None:
        if args.detail == "all" or args.verbose > 0:
            print("[TUNNEL STATE]")
            if IfStaName != None:
                print("Tunnel interface:", IfStaName.text)
            if IfStaAdmin != None:
                TunIsAdminUp = True
                print("  Admin status:", IfStaAdmin.text)
            if IfStaOper != None:
                TunIsOperUp = True
                print("   Oper status:", IfStaOper.text)
            print("")

    # print bgp state
    if BgpNeighborState != None:
        if args.detail == "all" or args.verbose > 0:
            print("[BGP STATE]")
            if BgpStaNeiId != None:
                print("BGP neighbor:", BgpStaNeiId.text)
            if BgpStaDesc != None:
                print("  Description:", BgpStaDesc.text)
            if BgpStaSess != None:
                print("  Session state:", BgpStaSess.text)
                if BgpStaSess.text == "fsm-established":
                    if BgpStaUpTime != None:
                        print("  Uptime:", BgpStaUpTime.text)
                    if BgpStaPfxSent != None:
                        print("  Prefixes sent:", BgpStaPfxSent.text)
                    if BgpStaPfxRcvd != None:
                        print("  Prefixes received:", BgpStaPfxRcvd.text)
                    if BgpStaPfxInst != None:
                        print("  Prefixes installed:", BgpStaPfxInst.text)
            print("")

    # print condensed (brief) output of most important info
    if args.detail == "brief" and args.verbose == 0:
        print("Tunnel_Int Admin Oper  Tunnel_Src      Tunnel_Dst      IPSEC_Profile")

        if TunIsConfigured == True:
            ipsecprofile = IfTunProtIpsecProf.text
            tundest = IfTunDst.text
        else:
            ipsecprofile = "Not configured"
            tundest = "Not configured"

        if TunIsShut == True:
            adminstate = "down"
            operstate = "down"
        else:
            adminstate = IfStaAdmin.text
            operstate = IfStaOper.text

        if IfName != None:
            print("{:<10}".format(IfName.text), "{:<5}".format(adminstate), "{:<5}".format(operstate), "{:<15}".format(IfTunSrc.text), "{:<15}".format(tundest), "{:<32}".format(ipsecprofile),"\n")
        else:
            print("{:<10}".format("Not conf'd"), "{:<5}".format("N/A"), "{:<5}".format("N/A"), "{:<15}".format("N/A"), "{:<15}".format("N/A"), "{:<32}".format("N/A"),"\n")

        if BgpIsConfigured != True:
            bgpnei = "Not conf'd"
            bgpsess = "N/A"
            pfxsent = "N/A"
            pfxrcvd = "N/A"
            pfxinst = "N/A"
            bgpuptime = "N/A"
            bgppeergroup = "N/A"
        else:
            bgpnei = BgpStaNeiId.text
            bgppeergroup = BgpPeerGroup.text

            if BgpNeiIsUp == True:
                bgpsess = "up"
                pfxsent = BgpStaPfxSent.text
                pfxrcvd = BgpStaPfxRcvd.text
                pfxinst = BgpStaPfxInst.text
                bgpuptime = BgpStaUpTime.text
            else:
                bgpsess = "down"
                pfxsent = "N/A"
                pfxrcvd = "N/A"
                pfxinst = "N/A"
                bgpuptime = "N/A"

        print("BGP_Neighbor    Peer_Group           State Sent    Rcvd    Inst    Uptime")
        print("{:<15}".format(bgpnei), "{:<20}".format(bgppeergroup), "{:<5}".format(bgpsess), "{:<7}".format(pfxsent), "{:<7}".format(pfxrcvd), "{:<7}".format(pfxinst), "{:<20}".format(bgpuptime), "\n")

