#
#        Name: rcnatctl.py
# Description: RHEDCloud NAT Control
#              Add/check/delete static NAT entries via NETCONF/YANG
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 03/20/2020
#
import sys
import lxml.etree as ET
from argparse import ArgumentParser
from ncclient import manager
from ncclient.operations import RPCError
from rcnet import Load_RPC_Template, Finalize_RPC, Lock_DS, Discard_Config, Commit_Config, Edit_Config, NETCONF_Datastore, YANG_Version

if __name__ == '__main__':

    # Number of configuration lock retry attempts
    LockRetries = 50
    # Number of seconds to sleep between attempts
    RetryCooldown = 10

    parser = ArgumentParser(description='Usage:')

    # script arguments
    parser.add_argument('-a', '--host', type=str, required=True,
                        help="Device IP address or Hostname.  Allowed values are: <r1 | r2")
    parser.add_argument('-u', '--username', type=str, required=True,
                        help="Device Username (netconf agent username)")
    parser.add_argument('-p', '--password', type=str, required=True,
                        help="Device Password (netconf agent password)")
    parser.add_argument('--port', type=int, default=830,
                        help="Netconf agent port")
    parser.add_argument('--action', type=str, required=True,
                        help="Action to take <add | delete | status>")
    parser.add_argument('--LocalIpAddress', type=str, required=True,
                        help="RFC1918 private IP address")
    parser.add_argument('--GlobalIpAddress', type=str, required=True,
                        help="Public IP address")
    parser.add_argument('--verbose', type=str, required=False,
                        help="Set to 1 for more detailed output")
    args = parser.parse_args()

    if args.host.lower().startswith("r1"):
      TunnelNum = "1"
    if args.host.lower().startswith("r2"):
      TunnelNum = "2"

    if TunnelNum == "0":
      sys.exit("Invalid HOST argument provided.  Run with -h for valid options.") 

    # print verbose info
    if args.verbose > 0:
      print(args.host)
      print(args.LocalIpAddress)
      print(args.GlobalIpAddress)

    # connect to netconf agent
    with manager.connect(host=args.host,
                         port=args.port,
                         username=args.username,
                         password=args.password,
                         timeout=90,
                         hostkey_verify=False,
                         device_params={'name': 'csr'}) as m:

        # determine yang model version 
        IOS_XE_YANG_VERSION = YANG_Version(m, args) 

        #
        # perform string replacements in RPC templates to make them specific for this NAT entry
        #
        if args.action == "add":
            RPC = Load_RPC_Template(m, "add", args, IOS_XE_YANG_VERSION, "NAT", None)
            RPC = Finalize_RPC("MODIFY", RPC)

        if args.action == "delete":
            RPC = Load_RPC_Template(m, "delete", args, IOS_XE_YANG_VERSION, "NAT", None)
            RPC = Finalize_RPC("MODIFY", RPC)

        if args.action == "status":
            RPC = Load_RPC_Template(m, "status", args, IOS_XE_YANG_VERSION, "NAT", None)
            RPC = Finalize_RPC("QUERY", RPC)

        RPC = RPC.replace("${LocalIpAddress}", args.LocalIpAddress)
        RPC = RPC.replace("${GlobalIpAddress}", args.GlobalIpAddress)

        #print RPC
        #sys.exit()

        # determine which datastore to use
        DATASTORE = NETCONF_Datastore(m, args)

        # execute netconf operation
        if args.action == "add":
            if DATASTORE == "candidate":
                Lock_DS(m, "candidate", LockRetries, RetryCooldown)
                Lock_DS(m, "running", LockRetries, RetryCooldown)
            else:
                Lock_DS(m, "running", LockRetries, RetryCooldown)

            print "Configuring NAT."
            Edit_Config(m, DATASTORE, RPC, args)

            if DATASTORE == "candidate":
                print "Releasing locks."
                m.unlock("running")
                m.unlock("candidate")
            else:
                print "Releasing lock."
                m.unlock("running")

        elif args.action == "delete":
            if DATASTORE == "candidate":
                Lock_DS(m, "candidate", LockRetries, RetryCooldown)
                Lock_DS(m, "running", LockRetries, RetryCooldown)
            else:
                Lock_DS(m, "running", LockRetries, RetryCooldown)

            print "Removing NAT."
            Edit_Config(m, DATASTORE, RPC, args)

            if DATASTORE == "candidate":
                print "Releasing locks."
                m.unlock("running")
                m.unlock("candidate")
            else:
                print "Releasing lock."
                m.unlock("running")

        elif args.action == "status":
          try:
              if args.verbose > 0:
                  print(RPC)
              # Get - can retrieve config data as well as device state data
              response = m.get(filter=RPC).xml
              data = ET.fromstring(response)
              print(ET.tostring(data, pretty_print=True))
          except RPCError as e:
              #data = e._raw
              #print(ET.tostring(data, pretty_print=True))
              print e.message

