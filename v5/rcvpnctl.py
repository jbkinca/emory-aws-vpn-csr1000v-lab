#
#        Name: rcvpnctl.py
# Description: RHEDCloud VPN Control
#              Add/check/delete VPNs and static NAT entries via NETCONF/YANG
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#
import sys
import lxml.etree as ET
import argparse
from ncclient import manager
from ncclient.operations import RPCError
from rcnet import Load_RPC_Template, Finalize_RPC, Lock_DS, Discard_Config, Commit_Config, Edit_Config, NETCONF_Datastore, YANG_Version, Get_Util, Print_Status

if __name__ == '__main__':
    # Number of configuration lock retry attempts
    LockRetries = 50
    # Number of seconds to sleep between attempts
    RetryCooldown = 10

    parser = argparse.ArgumentParser(add_help=False)
    child_parser = argparse.ArgumentParser(description='Usage:', parents=[parser])
    #
    # Common arguments
    #
    child_parser.add_argument('-a', '--host', type=str, required=True,
                        help="Device IP address or Hostname.")
    child_parser.add_argument('-u', '--username', type=str, required=True,
                        help="Device Username (netconf agent username)")
    child_parser.add_argument('-p', '--password', type=str, required=True,
                        help="Device Password (netconf agent password)")
    child_parser.add_argument('--port', type=int, default=830,
                        help="Netconf agent port")
    child_parser.add_argument('-d', '--device', type=int, required=True, choices=[1,2],
                        help="Device designation.  Allowed values are: <1 | 2>")
    child_parser.add_argument('--VpcNum', type=int, required=True,
                        help="VPN Profile Number <1-200>")
    child_parser.add_argument('--verbose', type=int, required=False, choices=[0,1], default=0,
                        help="Set to 1 for more detailed output")

    commands_subparser = child_parser.add_subparsers(title="commands", dest="action")
    commands_subparser.required = True

    #
    # ADD
    #
    add_command = commands_subparser.add_parser('add', help='Add provisioning to the router')

    csp_add_command_subparser = add_command.add_subparsers(title="CSP Selection", dest="Csp")
    csp_add_command_subparser.required = True

    # AWS specific tunnel options here
    aws_add_command = csp_add_command_subparser.add_parser('AWS', help='Amazon Web Services')
    aws_add_command.add_argument('--VpcId', type=str, required=True,
                        help="VPC identification string i.e. vpc-xxxxxxx")
    aws_add_command.add_argument('--Tunnels', type=str, required=False, default="AB", choices=['A', 'B', 'AB'],
                        help="Tunnels to act upon <A | B | AB> (defaults to AB)")

    aws_add_command.add_argument('--LocalVpnIpAddressA', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel A")
    aws_add_command.add_argument('--RemoteVpnIpAddressA', type=str, required=False,
                        help="CSP VPC VPN endpoint IP address Tunnel A")
    aws_add_command.add_argument('--PresharedKeyA', type=str, required=False,
                        help="CSP VPC VPN pre-shared key Tunnel A")
    aws_add_command.add_argument('--BgpNeighborIdA', type=str, required=False,
                        help="CSP BGP Neighbor IP address Tunnel A")

    aws_add_command.add_argument('--LocalVpnIpAddressB', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel B")
    aws_add_command.add_argument('--RemoteVpnIpAddressB', type=str, required=False,
                        help="CSP VPC VPN endpoint IP address Tunnel B")
    aws_add_command.add_argument('--PresharedKeyB', type=str, required=False,
                        help="CSP VPC VPN pre-shared key Tunnel B")
    aws_add_command.add_argument('--BgpNeighborIdB', type=str, required=False,
                        help="CSP BGP Neighbor IP address Tunnel B")

    # GCP specific tunnel options here
    gcp_add_command = csp_add_command_subparser.add_parser('GCP', help='Google Cloud Platform')
    gcp_add_command.add_argument('--VpcId', type=str, required=True,
                        help="VPC identification string i.e. vpc-xxxxxxx")
    gcp_add_command.add_argument('--Tunnels', type=str, required=False, default="A", choices=['A'],
                        help="Tunnels to act upon <A>")
    gcp_add_command.add_argument('--LocalVpnIpAddressA', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel A")
    gcp_add_command.add_argument('--RemoteVpnIpAddressA', type=str, required=False,
                        help="CSP VPC VPN endpoint IP address Tunnel A")
    gcp_add_command.add_argument('--PresharedKeyA', type=str, required=False,
                        help="CSP VPC VPN pre-shared key Tunnel A")
    gcp_add_command.add_argument('--BgpNeighborIdA', type=str, required=False,
                        help="CSP BGP Neighbor IP address Tunnel A")
    #
    # DELETE
    #
    delete_command = commands_subparser.add_parser('delete', help='Delete provisioning from the router')

    csp_delete_command_subparser = delete_command.add_subparsers(title="CSP Selection", dest="Csp")
    csp_delete_command_subparser.required = True

    # AWS specific tunnel options here
    aws_delete_command = csp_delete_command_subparser.add_parser('AWS', help='Amazon Web Services')
    aws_delete_command.add_argument('--VpcId', type=str, required=True,
                        help="VPC identification string i.e. vpc-xxxxxxx")
    aws_delete_command.add_argument('--Tunnels', type=str, required=False, default="AB", choices=['A', 'B', 'AB'],
                        help="Tunnels to act upon <A | B | AB> (defaults to AB)")

    aws_delete_command.add_argument('--LocalVpnIpAddressA', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel A")
    aws_delete_command.add_argument('--RemoteVpnIpAddressA', type=str, required=False,
                        help="CSP VPC VPN endpoint IP address Tunnel A")
    aws_delete_command.add_argument('--PresharedKeyA', type=str, required=False,
                        help="CSP VPC VPN pre-shared key Tunnel A")
    aws_delete_command.add_argument('--BgpNeighborIdA', type=str, required=False,
                        help="CSP BGP Neighbor IP address Tunnel A")

    aws_delete_command.add_argument('--LocalVpnIpAddressB', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel B")
    aws_delete_command.add_argument('--RemoteVpnIpAddressB', type=str, required=False,
                        help="CSP VPC VPN endpoint IP address Tunnel B")
    aws_delete_command.add_argument('--PresharedKeyB', type=str, required=False,
                        help="CSP VPC VPN pre-shared key Tunnel B")
    aws_delete_command.add_argument('--BgpNeighborIdB', type=str, required=False,
                        help="CSP BGP Neighbor IP address Tunnel B")

    # GCP specific tunnel options here
    gcp_delete_command = csp_delete_command_subparser.add_parser('GCP', help='Google Cloud Platform')
    gcp_delete_command.add_argument('--VpcId', type=str, required=True,
                        help="VPC identification string i.e. vpc-xxxxxxx")
    gcp_delete_command.add_argument('--Tunnels', type=str, required=False, default="A", choices=['A'],
                        help="Tunnels to act upon <A>")

    gcp_delete_command.add_argument('--LocalVpnIpAddressA', type=str, required=False,
                        help="Your VPN endpoint IP address Tunnel A")
    gcp_delete_command.add_argument('--RemoteVpnIpAddressA', type=str, required=False,
                        help="CSP VPC VPN endpoint IP address Tunnel A")
    gcp_delete_command.add_argument('--PresharedKeyA', type=str, required=False,
                        help="CSP VPC VPN pre-shared key Tunnel A")
    gcp_delete_command.add_argument('--BgpNeighborIdA', type=str, required=False,
                        help="CSP BGP Neighbor IP address Tunnel A")

    #
    # STATUS
    #
    status_command = commands_subparser.add_parser('status', help='View provisioning status and state')
    status_command.add_argument('--detail', type=str, required=False, choices=['brief', 'all'], default='all',
                        help="How much detail to show in status output")

    #print child_parser.parse_args()
    args = child_parser.parse_args()

    #
    # Determine Tunnel Number based on host
    #
    TunnelNum = "0"
    if args.device == 1:
      TunnelNum = "1"
    if args.device == 2:
      TunnelNum = "2"
    if TunnelNum == "0":
      sys.exit("Invalid device argument provided.  Run with -h for valid options.") 

    # create padded VpcNum str i.e. 001, 002, etc.
    VpcNum=str(args.VpcNum).zfill(3)

    # TunnelId
    TunnelIdA = TunnelNum + "0" + VpcNum
    TunnelIdB = TunnelNum + "1" + VpcNum

    # print verbose info
    if args.verbose > 0:
      print(args.host)
      print(VpcNum)
      print(TunnelNum)
      print(TunnelIdA)
      print(TunnelIdB)
      if(args.action == "add" or args.action == "delete"):
          print(args.Csp)
          if(args.Tunnels == "A" or args.Tunnels == "AB"):
              print(args.LocalVpnIpAddressA)
              print(args.RemoteVpnIpAddressA)
              print(args.PresharedKeyA)
              print(args.BgpNeighborIdA)
          if(args.Tunnels == "B" or args.Tunnels == "AB"):
              print(args.LocalVpnIpAddressB)
              print(args.RemoteVpnIpAddressB)
              print(args.PresharedKeyB)
              print(args.BgpNeighborIdB)

    # connect to netconf agent
    with manager.connect(host=args.host,
                         port=args.port,
                         username=args.username,
                         password=args.password,
                         timeout=90,
                         hostkey_verify=False,
                         device_params={'name': 'csr'}) as m:

        # determine yang model version 
        IOS_XE_YANG_VERSION = YANG_Version(m, args) 

        # determine local BGP ASN
        RPC = Load_RPC_Template(m, "status", args, IOS_XE_YANG_VERSION, "ASN", None)
        RPC = Finalize_RPC("QUERY", RPC)
        MyBgpAsn = Get_Util(m, args, IOS_XE_YANG_VERSION, RPC, "ASN", None)

        # determine which datastore to use
        DATASTORE = NETCONF_Datastore(m, args) 

        RPC_A = ""
        RPC_B = ""
        RPC_A_s = ""
        RPC_B_s = ""

        # execute netconf operation
        if args.action == "add":
            #
            # perform string replacements in RPC templates to make them specific to a VPN
            #
            if args.Tunnels == "A" or args.Tunnels == "AB":
                RPC_A = Load_RPC_Template(m, "add", args, IOS_XE_YANG_VERSION, "VPN", None)
                RPC_A = RPC_A.replace("${TunnelId}", TunnelIdA)
                RPC_A = RPC_A.replace("${LocalVpnIpAddress}", args.LocalVpnIpAddressA)
                RPC_A = RPC_A.replace("${RemoteVpnIpAddress}", args.RemoteVpnIpAddressA)
                RPC_A = RPC_A.replace("${PresharedKey}", args.PresharedKeyA)
                RPC_A = RPC_A.replace("${BgpNeighborId}", args.BgpNeighborIdA)
                RPC_A = RPC_A.replace("${VpcId}", args.VpcId)
                RPC_A = RPC_A.replace("${TunnelNumber}", TunnelNum)
                RPC_A = RPC_A.replace("${VpnId-Padded}", VpcNum)
                RPC_A = RPC_A.replace("${MyBgpAsn}", MyBgpAsn)

            if args.Tunnels == "B" or args.Tunnels == "AB":
                RPC_B = Load_RPC_Template(m, "add", args, IOS_XE_YANG_VERSION, "VPN", None)
                RPC_B = RPC_B.replace("${TunnelId}", TunnelIdB)
                RPC_B = RPC_B.replace("${LocalVpnIpAddress}", args.LocalVpnIpAddressB)
                RPC_B = RPC_B.replace("${RemoteVpnIpAddress}", args.RemoteVpnIpAddressB)
                RPC_B = RPC_B.replace("${PresharedKey}", args.PresharedKeyB)
                RPC_B = RPC_B.replace("${BgpNeighborId}", args.BgpNeighborIdB)
                RPC_B = RPC_B.replace("${VpcId}", args.VpcId)
                RPC_B = RPC_B.replace("${TunnelNumber}", TunnelNum + "B")
                RPC_B = RPC_B.replace("${VpnId-Padded}", VpcNum)
                RPC_B = RPC_B.replace("${MyBgpAsn}", MyBgpAsn)

            RPC = Finalize_RPC("MODIFY", RPC_A + RPC_B)
            #print RPC
            if DATASTORE == "candidate":
                Lock_DS(m, "candidate", LockRetries, RetryCooldown)
                Lock_DS(m, "running", LockRetries, RetryCooldown)
            else:
                Lock_DS(m, "running", LockRetries, RetryCooldown)

            print "Step 1 of 1: Configuring VPN."
            Edit_Config(m, DATASTORE, RPC, args)

            if DATASTORE == "candidate":
                print "Releasing locks."
                m.unlock("running")
                m.unlock("candidate")
            else:
                print "Releasing lock."
                m.unlock("running")

        elif args.action == "delete":
            #
            # perform string replacements in RPC templates to make them specific to a VPN
            #
            if args.Tunnels == "A" or args.Tunnels == "AB":
                RPC_A_s = Load_RPC_Template(m, "shutdown", args, IOS_XE_YANG_VERSION, "VPN", None) 
                RPC_A_s = RPC_A_s.replace("${TunnelId}", TunnelIdA)
                RPC_A_s = RPC_A_s.replace("${LocalVpnIpAddress}", args.LocalVpnIpAddressA)
                RPC_A_s = RPC_A_s.replace("${RemoteVpnIpAddress}", args.RemoteVpnIpAddressA)
                RPC_A_s = RPC_A_s.replace("${PresharedKey}", args.PresharedKeyA)
                RPC_A_s = RPC_A_s.replace("${VpcId}", args.VpcId)
                RPC_A_s = RPC_A_s.replace("${TunnelNumber}", TunnelNum)
                RPC_A_s = RPC_A_s.replace("${VpnId-Padded}", VpcNum)
                RPC_A_s = RPC_A_s.replace("${BgpNeighborId}", args.BgpNeighborIdA)
                RPC_A_s = RPC_A_s.replace("${MyBgpAsn}", MyBgpAsn)
   
                RPC_A = Load_RPC_Template(m, "delete", args, IOS_XE_YANG_VERSION, "VPN", None)
                RPC_A = RPC_A.replace("${TunnelId}", TunnelIdA)
                RPC_A = RPC_A.replace("${LocalVpnIpAddress}", args.LocalVpnIpAddressA)
                RPC_A = RPC_A.replace("${RemoteVpnIpAddress}", args.RemoteVpnIpAddressA)
                RPC_A = RPC_A.replace("${PresharedKey}", args.PresharedKeyA)
                RPC_A = RPC_A.replace("${VpcId}", args.VpcId)
                RPC_A = RPC_A.replace("${TunnelNumber}", TunnelNum)
                RPC_A = RPC_A.replace("${VpnId-Padded}", VpcNum)
                RPC_A = RPC_A.replace("${BgpNeighborId}", args.BgpNeighborIdA)
                RPC_A = RPC_A.replace("${MyBgpAsn}", MyBgpAsn)

            if args.Tunnels == "B" or args.Tunnels == "AB":
                RPC_B_s = Load_RPC_Template(m, "shutdown", args, IOS_XE_YANG_VERSION, "VPN", None)
                RPC_B_s = RPC_B_s.replace("${TunnelId}", TunnelIdB)
                RPC_B_s = RPC_B_s.replace("${LocalVpnIpAddress}", args.LocalVpnIpAddressB)
                RPC_B_s = RPC_B_s.replace("${RemoteVpnIpAddress}", args.RemoteVpnIpAddressB)
                RPC_B_s = RPC_B_s.replace("${PresharedKey}", args.PresharedKeyB)
                RPC_B_s = RPC_B_s.replace("${VpcId}", args.VpcId)
                RPC_B_s = RPC_B_s.replace("${TunnelNumber}", TunnelNum + "B")
                RPC_B_s = RPC_B_s.replace("${VpnId-Padded}", VpcNum)
                RPC_B_s = RPC_B_s.replace("${BgpNeighborId}", args.BgpNeighborIdB)
                RPC_B_s = RPC_B_s.replace("${MyBgpAsn}", MyBgpAsn)

                RPC_B = Load_RPC_Template(m, "delete", args, IOS_XE_YANG_VERSION, "VPN", None)
                RPC_B = RPC_B.replace("${TunnelId}", TunnelIdB)
                RPC_B = RPC_B.replace("${LocalVpnIpAddress}", args.LocalVpnIpAddressB)
                RPC_B = RPC_B.replace("${RemoteVpnIpAddress}", args.RemoteVpnIpAddressB)
                RPC_B = RPC_B.replace("${PresharedKey}", args.PresharedKeyB)
                RPC_B = RPC_B.replace("${VpcId}", args.VpcId)
                RPC_B = RPC_B.replace("${TunnelNumber}", TunnelNum + "B")
                RPC_B = RPC_B.replace("${VpnId-Padded}", VpcNum)
                RPC_B = RPC_B.replace("${BgpNeighborId}", args.BgpNeighborIdB)
                RPC_B = RPC_B.replace("${MyBgpAsn}", MyBgpAsn)

            RPC_s = Finalize_RPC("MODIFY", RPC_A_s + RPC_B_s)
            #print RPC_s
            RPC = Finalize_RPC("MODIFY", RPC_A + RPC_B)
            #print RPC

            if DATASTORE == "candidate":
                Lock_DS(m, "candidate", LockRetries, RetryCooldown)
                Lock_DS(m, "running", LockRetries, RetryCooldown)
            else:
                Lock_DS(m, "running", LockRetries, RetryCooldown)

            print "Step 1 of 2: Shutting down tunnel interface."
            Edit_Config(m, DATASTORE, RPC_s, args)

            print "Step 2 of 2: Resetting VPN configuration to defaults."
            Edit_Config(m, DATASTORE, RPC, args)

            if DATASTORE == "candidate":
                print "Releasing locks."
                m.unlock("running")
                m.unlock("candidate")
            else:
                print "Releasing lock."
                m.unlock("running")

        elif args.action == "status":
            # first determine CSP from the router
            RPC_A = Load_RPC_Template(m, "status", args, IOS_XE_YANG_VERSION, "Tunnel", None)
            RPC_A = RPC_A.replace("${TunnelId}", TunnelIdA)

            RPC_B = Load_RPC_Template(m, "status", args, IOS_XE_YANG_VERSION, "Tunnel", None)
            RPC_B = RPC_B.replace("${TunnelId}", TunnelIdB)

            RPC = Finalize_RPC("QUERY", RPC_A + RPC_B)

            CspA = Get_Util(m, args, IOS_XE_YANG_VERSION, RPC, "CSP", TunnelIdA)
            CspB = Get_Util(m, args, IOS_XE_YANG_VERSION, RPC, "CSP", TunnelIdB)

            BgpNeighborIdA = Get_Util(m, args, IOS_XE_YANG_VERSION, RPC, "BgpNeighborId", TunnelIdA)
            BgpNeighborIdB = Get_Util(m, args, IOS_XE_YANG_VERSION, RPC, "BgpNeighborId", TunnelIdB)

            if CspA != None and CspB != None and CspA != CspB:
                print "WARNING - CSP differs between Tunnels A & B!"

            #
            # perform string replacements in RPC templates to make them specific to a VPN
            #
            if CspA != None:
                RPC_A = Load_RPC_Template(m, "status", args, IOS_XE_YANG_VERSION, "VPN", CspA)
                RPC_A = RPC_A.replace("${TunnelId}", TunnelIdA)
                RPC_A = RPC_A.replace("${TunnelNumber}", TunnelNum)
                RPC_A = RPC_A.replace("${VpnId-Padded}", VpcNum)
                RPC_A = RPC_A.replace("${BgpNeighborId}", BgpNeighborIdA)
                RPC_A = RPC_A.replace("${MyBgpAsn}", MyBgpAsn)

            if CspB != None:
                RPC_B = Load_RPC_Template(m, "status", args, IOS_XE_YANG_VERSION, "VPN", CspB)
                RPC_B = RPC_B.replace("${TunnelId}", TunnelIdB)
                RPC_B = RPC_B.replace("${TunnelNumber}", TunnelNum + "B")
                RPC_B = RPC_B.replace("${VpnId-Padded}", VpcNum)
                RPC_B = RPC_B.replace("${BgpNeighborId}", BgpNeighborIdB)
                RPC_B = RPC_B.replace("${MyBgpAsn}", MyBgpAsn)

            RPC = Finalize_RPC("QUERY", RPC_A + RPC_B)
            #print RPC

            try:
                if args.verbose > 0:
                    print(RPC)

                response = m.get(filter=RPC).xml
                root = ET.fromstring(response)
                if args.verbose > 0:
                    print(ET.tostring(root, pretty_print=True))
             
                #
                # Remove XML namespace prefixes
                # This is required in order to use ET.find() or ET.findall()
                #
                for elem in root.getiterator():
                    elem.tag = ET.QName(elem).localname

                ET.cleanup_namespaces(root)

                # Uncomment to see XML post namespace removal
                #print(ET.tostring(root, pretty_print=True).decode())

                for AorB in ['A', 'B']: 
                    BgpNeighborState = None
                    TunnelIfState = None
                    TunnelIfConf = None
                    CryptoConf = None
                    BgpConf = None

                    if AorB == "A":
                        BgpNeighborId = BgpNeighborIdA
                        TunnelId = TunnelIdA
                        Csp = CspA
                    if AorB == "B":
                        BgpNeighborId = BgpNeighborIdB
                        TunnelId = TunnelIdB
                        Csp = CspB

                    if args.detail == "all":
                        print "Router: " + TunnelNum
                        if Csp != None:
                            print "CSP: " + Csp
                        else:
                            print "CSP: N/A"

                        print "Tunnel: " + AorB

                    # select BGP configuration data for this tunnel's neighbor
                    if BgpNeighborId != None:
                        if IOS_XE_YANG_VERSION == "1662":
                            BgpConf = root.find("./data/native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor[id='" + BgpNeighborId + "']../../../../..")
                        elif IOS_XE_YANG_VERSION == "1693":
                            BgpConf = root.find("./data/native/router/bgp/address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor[id='" + BgpNeighborId + "']../../../../../..")
                    #if BgpConf != None:
                        #print(ET.tostring(BgpConf, pretty_print=True))

                    if BgpConf == None and args.detail == "all":
                        print "WARNING - unable to locate bgp neighbor configuration."

                    # select Tunnel BGP state data
                    if BgpNeighborId != None:
                        BgpNeighborState = root.find("./data/bgp-state-data/neighbors/neighbor[neighbor-id='" + BgpNeighborId + "']")
                        #print(ET.tostring(BgpNeighborState, pretty_print=True))
                    else:
                        if args.detail == "all":
                            print "WARNING - unable to locate bgp state data."

                    # select Tunnel state data
                    TunnelIfState = root.find("./data/interfaces-state/interface[name='Tunnel" + TunnelId + "']")
                    #print(ET.tostring(TunnelIfState, pretty_print=True))

                    if TunnelIfState == None and args.detail == "all":
                        print "WARNING - unable to locate tunnel interface state data."

                    # select Tunnel interface configuration data
                    TunnelIfConf = root.find("./data/native/interface/Tunnel[name='" + TunnelId + "']")
                    #print(ET.tostring(TunnelIfConf, pretty_print=True))

                    if TunnelIfConf == None:
                        if args.detail == "all":
                            print "WARNING - unable to locate tunnel interface config."

                    else:
                        # get ipsec profile name that is applied to tunnel interface
                        IpsecProfileName = TunnelIfConf.find("./tunnel/protection/ipsec/profile")

                        if IpsecProfileName == None:
                            if args.detail == "all":
                                print "WARNING - tunnel protection is not configured."
    
                        else:
                            #print IpsecProfileName.text

                            # select Tunnel crypto configuration data
                            CryptoConf = root.find("./data/native/crypto/ipsec/profile[name='" + IpsecProfileName.text + "']../..")

                            if CryptoConf == None and args.detail == "all":
                                # print state for Tunnel
                                print "WARNING - unable to locate crypto config."
                            #elif CryptoConf != None:
                                #print(ET.tostring(CryptoConf, pretty_print=True))

                    Print_Status(args, IOS_XE_YANG_VERSION, TunnelNum, BgpNeighborState, TunnelIfState, TunnelIfConf, CryptoConf, BgpConf)

            except RPCError as e:
                #root = e._raw
                #print(ET.tostring(root, pretty_print=True))
                print e.message

