#
#        Name: wrapper.sh
# Description: bash wrapper script for provisioning multiple AWS IPSEC VPNs in the virtual lab environment.
#              Calls easvpnctl.py, but values to arguments are only valid in the lab.
#      Author: Jimmy B. Kincaid
#       Email: jimmy.kincaid@emory.edu
#        Date: 10/25/2018
#

#
# $1 = Action to perform - <add | delete | status>
#
# $2 = First VPC in the loop - (integer >= 1)
#
# $3 = Last VPC in the loop - (integer <= 200, can be same as $2 if only touching a single VPC)
#
# $4 = Tunnels to act on <A | B | AB>

#
# edit these to match your setup
#
Router1="r1"
Router2="r2"
user="admin"
password="cisco"
MyBgpAsn="65000"

#
# Don't edit below this line
#
RemoteVpnIpBase1="10.1"
RemoteVpnIpBase2="10.2"

for i in `seq $2 $3`; do
  VpcId="vpc-`printf "%03d\n" ${i}`"
  RemoteVpnIpAddress1A=${RemoteVpnIpBase1}.0.${i}
  RemoteVpnIpAddress1B=${RemoteVpnIpBase1}.1.${i}
  RemoteVpnIpAddress2A=${RemoteVpnIpBase2}.0.${i}
  RemoteVpnIpAddress2B=${RemoteVpnIpBase2}.1.${i}
  PresharedKey1A="test`printf "%03d\n" ${i}`-1a"
  PresharedKey1B="test`printf "%03d\n" ${i}`-1b"
  PresharedKey2A="test`printf "%03d\n" ${i}`-2a"
  PresharedKey2B="test`printf "%03d\n" ${i}`-2b"

  echo "Provisioning VPC $i VPN 1 ${4} - $1"
  python easvpnctl.py -a ${Router1} -u ${user} -p ${password} --RemoteVpnIpAddressA ${RemoteVpnIpAddress1A} --PresharedKeyA ${PresharedKey1A} --RemoteVpnIpAddressB ${RemoteVpnIpAddress1B} --PresharedKeyB ${PresharedKey1B} --VpcId ${VpcId} --VpcNum ${i} --action ${1} --Tunnels ${4} --MyBgpAsn ${MyBgpAsn}

  echo "Provisioning VPC $i VPN 2 ${4} - $1"
  python easvpnctl.py -a ${Router2} -u ${user} -p ${password} --RemoteVpnIpAddressA ${RemoteVpnIpAddress2A} --PresharedKeyA ${PresharedKey2A} --RemoteVpnIpAddressB ${RemoteVpnIpAddress2B} --PresharedKeyB ${PresharedKey2B} --VpcId ${VpcId} --VpcNum ${i} --action ${1} --Tunnels ${4} --MyBgpAsn ${MyBgpAsn}
done

